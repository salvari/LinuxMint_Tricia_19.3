# Introducción

Mi portátil es un Lenovo Legion con las siguientes características:

  - Core i7

  - NVIDIA Geforce GTX

  - 16Gb RAM

  - 500Gb ssd

# Programas básicos

## Linux Mint

Linux Mint incluye `sudo` \[1\] y las aplicaciones que uso habitualmente
para gestión de paquetes por defecto (*aptitude* y *synaptic*).

Tampoco voy a enredar nada con los orígenes del software (de momento)

## Firmware

Ya no es necesario intalar los paquetes de *microcode* la instalación de
Tricia se encargó de instalar:

  - `amd64-microcode`
  - `intel-microcode`

Instalamos el driver recomendado de nvidia desde el gestor de drivers
del *Linux Mint*. Ahora mismo es el *nvidia-driver-435*

Una vez instalado el driver de nvidia, el comando `prime-select
query`debe indicarnos la tarjeta activa y podremos cambiar de tarjeta
ejecutando `prime-select [nvidia|intel]`

## Control de configuraciones con git

Una vez instalado el driver de nvidia y antes de seguir con la
instalación instalamos el `git` y el `etckeeper` para que toda nuestra
instalación quede reflejada en los repos.

### Instalación de `etckeeper`

¡Ojo\!, nos hacemos `root` para ejecutar:

    sudo su -
    git config --global user.email xxxxx@whatever.com
    git config --global user.name "Name Surname"
    apt install etckeeper

*etckeeper* hara un control automático de tus ficheros de configuración
en `/etc`

Para echar una mirada a los *commits* creados puedes ejecutar:

    cd /etc
    sudo git log

### Controlar dotfiles con git

Vamos a crear un repo de git para controlar nuestros ficheros personales
de configuración.

Creamos el repo donde queramos

    mkdir usrcfg
    cd usrcfg
    git init
    git config core.worktree "/home/salvari"

Y ya lo tenemos, un repo que tiene el directorio de trabajo apuntando a
nuestro *$HOME*.

Podemos añadir los ficheros de configuración que queramos al repo:

    git add .bashrc
    git commit -m "Add some dotfiles"

Una vez que tenga añadidos los ficheros que quiero tener controlados
pondré `*` en el fichero `.git/info/exclude` de mi repo para que ignore
todos los ficheros de mi `$HOME`.

Cuando instalo algún programa nuevo añado a mano los ficheros de
configuración al repo.

## Parámetros de disco duro

Tengo un disco duro ssd y otro hdd normal.

El area de intercambio la hemos creado en el disco duro hdd, no se usará
mucho pero evitamos multiples operaciones de escritura en el disco ssd
en caso de que se empiece a tirar del swap.

Añadimos el parámetro `noatime` para las particiones de `root` y
`/home`, que si que se han creado en el ssd.

    # /etc/fstab: static file system information.
    #
    # Use 'blkid' to print the universally unique identifier for a
    # device; this may be used with UUID= as a more robust way to name devices
    # that works even if disks are added and removed. See fstab(5).
    #
    # <file system> <mount point>   <type>  <options>       <dump>  <pass>
    # / was on /dev/sda5 during installation
    UUID=d96a5501-75b9-4a25-8ecb-c84cd4a3fff5 /               ext4    noatime,errors=remount-ro 0       1
    # /home was on /dev/sda7 during installation
    UUID=8fcde9c5-d694-4417-adc0-8dc229299f4c /home           ext4    defaults,noatime        0       2
    # /store was on /dev/sdc7 during installation
    UUID=0f0892e0-9183-48bd-aab4-9014dc1bd03a /store          ext4    defaults        0       2
    # swap was on /dev/sda6 during installation
    UUID=ce11ccb0-a67d-4e8b-9456-f49a52974160 none            swap    sw              0       0
    # swap was on /dev/sdc5 during installation
    UUID=11090d84-ce98-40e2-b7be-dce3f841d7b4 none            swap    sw              0       0

Una vez modificado el `/etc/fstab` no hace falta arrancar, basta con
ejecutar lo siguiente:

    mount -o remount /
    mount -o remount /home
    mount

### Ajustar *Firefox*

Seguimos [esta
referencia](https://easylinuxtipsproject.blogspot.com/p/ssd.html#ID10)

Visitamos `about::config` con el navegador.

Cambiamos

  - `browser.cache.disk.enable` **false**
  - `browser.cache.memory.enable` **true**
  - `browser.cache.memory.capacity` **204800**
  - `browser.sessionstore.interval` **15000000**

TODO: Comprobar *trim* en mi disco duro. Y mirar
[esto](https://easylinuxtipsproject.blogspot.com/p/speed-mint.html)

## Fuentes adicionales

Instalamos algunas fuentes desde los orígenes de software:

    sudo apt install ttf-mscorefonts-installer
    sudo apt install fonts-noto

Y la fuente [Mensch](https://robey.lag.net/2010/06/21/mensch-font.html)
la bajamos directamente al directorio `~/.local/share/fonts`

## Firewall

`ufw` y `gufw` vienen instalados por defecto, pero no activados.

    aptitude install ufw
    ufw default deny
    ufw enable
    ufw status verbose
    aptitude install gufw

-----

> **Nota**: Ojo con el log de `ufw`, tampoco le sienta muy bien al ssd
> esa escritura masiva.

-----

## Aplicaciones variadas

> **Nota**: Ya no instalamos *menulibre*, Linux Mint tiene una utilidad
> de edición de menús.

  - Keepass2  
    Para mantener nuestras contraseñas a buen recaudo

  - Gnucash  
    Programa de contabilidad, la versión de los repos está bastante
    atrasada habrá que probar la nueva.

  - Deluge  
    Programa de descarga de torrents (acuérdate de configurar tus
    cortafuegos)

  - Chromium  
    Como Chrome pero libre

  - rsync, grsync  
    Para hacer backups de nuestros ficheros

  - Descompresores variados  
    Para lidiar con los distintos formatos de ficheros comprimidos

  - mc  
    Midnight Comander, gestor de ficheros en modo texto

  - most  
    Un `less` mejorado

<!-- end list -->

    sudo apt install keepass2 gnucash deluge rsync grsync rar unrar \
    zip unzip unace bzip2 lzop p7zip p7zip-full p7zip-rar chromium-browser\
    most mc

## Algunos programas de control

Son útiles para el lenovo.

    sudo apt install tlp tlp-rdw htop powertop

## Programas de terminal

Dos imprescindibles:

    sudo apt install guake terminator

**TODO:** asociar *Guake* a una combinación apropiada de teclas.

## Dropbox

Lo instalamos desde el software manager.

## Chrome

Instalado desde [la página web de
Chrome](https://www.google.com/chrome/)

## Varias aplicaciones instaladas de binarios

Lo recomendable en un sistema POSIX es instalar los programas
adicionales en `/usr/local` o en `/opt`. Yo soy más chapuzas y suelo
instalar en `~/apt` por que el portátil es personal e intrasferible. En
un ordenador compartido es mejor usar `/opt`.

### Freeplane

Para hacer mapas mentales, presentaciones, resúmenes, apuntes… La
versión incluida en LinuxMint está un poco anticuada.

1.  descargamos desde [la
    web](http://freeplane.sourceforge.net/wiki/index.php/Home).
2.  Descomprimimos en `~/apps/freeplane`
3.  Creamos enlace simbólico
4.  Añadimos a los menús

### Telegram Desktop

Cliente de Telegram, descargado desde la [página
web](https://desktop.telegram.org/).

### Tor browser

Descargamos desde la [página oficial del
proyecto](https://www.torproject.org/) Descomprimimos en `~/apps/` y
ejecutamos desde terminal:

    cd ~/apps/tor-browser
    ./start-tor-browser.desktop --register-app

### Brave browser

Instalamos siguiendo las instrucciones de la [página web
oficial](https://brave-browser.readthedocs.io/en/latest/installing-brave.html#linux)

``` {bash}
curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update

sudo apt install brave-browser
```

### TiddlyDesktop

Descargamos desde la [página
web](https://github.com/Jermolene/TiddlyDesktop), descomprimimos y
generamos la entrada en el menú.

### Joplin

Una herramienta libre para mantener notas sincronizadas entre el móvil y
el portátil.

La instalamos en el directorio `~/apps/joplin` descargando el AppImage
desde la [página web](https://joplinapp.org/)

Como siempre nos creamos una entrada en el menú.

## Terminal y Shell

Por defecto tenemos instalado `bash`.

### bash-git-promt

Seguimos las instrucciones de [este
github](https://github.com/magicmonty/bash-git-prompt)

### zsh

Nos adelantamos a los acontecimientos, pero conviene tener instaladas
las herramientas de entornos virtuales de python antes de instalar *zsh*
con el plugin para *virtualenvwrapper*.

    apt install python-all-dev
    apt install python3-all-dev
    apt install python-pip python-virtualenv virtualenv python3-pip
    apt install virtualenvwrapper

*zsh* viene por defecto en mi instalación, en caso contrario:

    apt install zsh

Para *zsh* vamos a usar [antigen](https://github.com/zsh-users/antigen),
así que nos lo clonamos en `~/apps/`

    cd ~/apps
    git clone https://github.com/zsh-users/antigen

También vamos a usar
[zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt), así
que lo clonamos también:

    cd ~/apps
    git clone https://github.com/olivierverdier/zsh-git-prompt)

Y editamos el fichero `~/.zshrc` para que contenga:

    # This line loads .profile, it's experimental
    [[ -e ~/.profile ]] && emulate sh -c 'source ~/.profile'
    
    source ~/apps/zsh-git-prompt/zshrc.sh
    source ~/apps/antigen/antigen.zsh
    
    # Load the oh-my-zsh's library.
    antigen use oh-my-zsh
    
    # Bundles from the default repo (robbyrussell's oh-my-zsh).
    antigen bundle git
    antigen bundle command-not-found
    
    # must install autojump for this
    #antigen bundle autojump
    
    # extracts every kind of compressed file
    antigen bundle extract
    
    # jump to dir used frequently
    antigen bundle z
    
    #antigen bundle pip
    
    antigen bundle common-aliases
    
    antigen bundle robbyrussell/oh-my-zsh plugins/virtualenvwrapper
    
    antigen bundle zsh-users/zsh-completions
    
    # Syntax highlighting bundle.
    antigen bundle zsh-users/zsh-syntax-highlighting
    antigen bundle zsh-users/zsh-history-substring-search ./zsh-history-substring-search.zsh
    
    # Arialdo Martini git needs awesome terminal font
    #antigen bundle arialdomartini/oh-my-git
    #antigen theme arialdomartini/oh-my-git-themes oppa-lana-style
    
    # autosuggestions
    antigen bundle tarruda/zsh-autosuggestions
    
    #antigen theme agnoster
    antigen theme gnzh
    
    # Tell antigen that you're done.
    antigen apply
    
    # Correct rm alias from common-alias bundle
    unalias rm
    alias rmi='rm -i'

Para usar *virtualenvwrapper* hay que decidir en que directorio queremos
salvar los entornos virtuales. El obvio seria `~/.virtualenvs` la
alternativa sería `~/.local/share/virtualenvs`.

El que escojamos lo tenemos que crear y añadirlo a nuestro `~/.profile`
con las líneas:

    # WORKON_HOME for virtualenvwrapper
    if [ -d "$HOME/.local/share/virtualenvs" ] ; then
        WORKON_HOME="$HOME/.local/share/virtualenvs"
    fi

*Antigen* ya se encarga de descargar todos los plugins que queramos
utilizar en zsh. Todos el software se descarga en `~/.antigen`

Para configurar el
[zsh-git-prompt](https://github.com/olivierverdier/zsh-git-prompt), que
inspiró el bash-git-prompt, he modificado el fichero `zshrc.sh` de
*zsh-git-prompt* y el fichero del tema *gnzh* en
`~/.antigen/bundles/robbyrussell/oh-my-zsh/themes/gnzh.zsh-theme`

Después de seguir estos pasos basta con arrancar el *zsh*

### fish

> **Nota**: No he instalado *fish* dejo por aquí las notas del antiguo
> linux mint por si le interesa a alguien.

Instalamos *fish*:

    sudo aptitude install fish

Instalamos oh-my-fish

    curl -L https://github.com/oh-my-fish/oh-my-fish/raw/master/bin/install > install
    fish install
    rm install

Si queremos que fish sea nuestro nuevo shell:

    chsh -s `which fish`

Los ficheros de configuración de *fish* se encuentran en
`~/config/fish`.

Los ficheros de *Oh-my-fish* en mi portátil quedan en
`~/.local/share/omf`

Para tener la info de git en el prompt de fish al estilo de
[bash-git-prompt](https://github.com/magicmonty/bash-git-prompt),
copiamos:

    cp ~/.bash-git-prompt/gitprompt.fish ~/.config/fish/functions/fish_prompt.fish

-----

> **NOTA**: *fish* es un shell estupendo, supercómodo, con un montón de
> funcionalidades. Pero no es POSIX. Mucho ojo con esto, usa *fish* pero
> aségurate de saber a que renuncias, o las complicaciones a las que vas
> a enfrentarte.

-----

### tmux

Esto no tiene mucho que ver con los shell, lo he instalado para aprender
a usarlo.

    sudo apt install tmux

[El tao de tmux](https://leanpub.com/the-tao-of-tmux/read)

-----

> **Nota**: Instalar *rxvt* junto con tmux como terminal alternativo

-----

## Utilidades

*Agave* y *pdftk* ya no existen, nos pasamos a *gpick* y
*poppler-utils*:

Instalamos *gpick* con `sudo apt install gpick`

## Codecs

    sudo apt-get install mint-meta-codecs

## Syncthing

Añadimos el ppa:

``` {bash}
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list


sudo apt-get update
sudo apt-get install syncthing
```

# Documentación

## Vanilla LaTeX

Para instalar la versión más reciente de LaTeX hay que aplicar este
truco.

    cd ~
    mkdir tmp
    cd tmp
    wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
    tar xzf install-tl-unx.tar.gz
    cd install-tl-xxxxxx

La parte xxxxxx varía en función del estado de la última versión de
LaTeX disponible.

    sudo ./install-tl

Una vez lanzada la instalación podemos desmarcar las opciones que
instalan la documentación y las fuentes. Eso nos obligará a consultar la
documentación on line pero ahorrará practicamente el 50% del espacio
necesario. En mi caso sin doc ni src ocupa 2,3Gb

    mkdir -p /opt/texbin
    sudo ln -s /usr/local/texlive/2018/bin/x86_64-linux/* /opt/texbin

Por último para acabar la instalación añadimos `/opt/texbin` al *PATH*.
Para *bash* y *zsh* basta con añadir al fichero `~/.profile` las
siguientes lineas:

    # adds texlive to my PATH
    if [ -d "/opt/texbin" ] ; then
        PATH="$PATH:/opt/texbin"
    fi

En cuanto a *fish* (si es que lo usas, claro) tendremos que modificar (o
crear) el fichero `~/.config/fish/config.fish` y añadir la siguiente
linea:

    set PATH $PATH /opt/texbin

### Falsificando paquetes

Ya tenemos el *texlive* instalado, ahora necesitamos que el gestor de
paquetes sepa que ya lo tenemos instalado.

    sudo apt install equivs --no-install-recommends
    mkdir -p /tmp/tl-equivs && cd /tmp/tl-equivs
    equivs-control texlive-local

Alternativamente para hacerlo más fácil podemos descargarnos un fichero
`texlive-local`ya preparado, ejecutando:

    wget http://www.tug.org/texlive/files/debian-equivs-2018-ex.txt
    /bin/cp -f debian-equivs-2018-ex.txt texlive-local

Editamos la versión (si queremos) y procedemos a generar el paquete
*deb*.

    equivs-build texlive-local

El paquete que hemos generado tiene una dependencia: *freeglut3*, hay
que instalarla previamente.

    sudo apt install freeglut3
    sudo dpkg -i texlive-local_2018-1_all.deb

Todo listo, ahora podemos instalar cualquier paquete debian que dependa
de *texlive* sin problemas de dependencias, aunque no hayamos instalado
el *texlive* de Debian.

### Fuentes

Para dejar disponibles las fuentes opentype y truetype que vienen con
texlive para el resto de aplicaciones:

    sudo cp $(kpsewhich -var-value TEXMFSYSVAR)/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf
    sudo nano /etc/fonts/conf.d/09-texlive.conf

Borramos la linea:

    <dir>/usr/local/texlive/20xx/texmf-dist/fonts/type1</dir>

Y ejecutamos:

    sudo fc-cache -fsv

Actualizaciones Para actualizar nuestro *latex* a la última versión de
todos los paquetes:

    sudo /opt/texbin/tlmgr update --self
    sudo /opt/texbin/tlmgr update --all

También podemos lanzar el instalador gráfico con:

    sudo /opt/texbin/tlmgr --gui

Para usar el instalador gráfico hay que instalar previamente:

    sudo apt-get install perl-tk --no-install-recommends

Lanzador para el actualizador de *texlive*:

    mkdir -p ~/.local/share/applications
    /bin/rm ~/.local/share/applications/tlmgr.desktop
    cat > ~/.local/share/applications/tlmgr.desktop << EOF
    [Desktop Entry]
    Version=1.0
    Name=TeX Live Manager
    Comment=Manage TeX Live packages
    GenericName=Package Manager
    Exec=gksu -d -S -D "TeX Live Manager" '/opt/texbin/tlmgr -gui'
    Terminal=false
    Type=Application
    Icon=system-software-update
    EOF

## Tipos de letra

Creamos el directorio de usuario para tipos de letra:

    mkdir ~/.local/share/fonts

## Fuentes Adicionales

Un par de fuentes las he descargado de internet y las he almacenado en
el directorio de usuario para los tipos de letra: `~/.local/share/fonts`

  - [Ubuntu](https://design.ubuntu.com/font/) (La uso en documentación)
  - [Mensch](https://robey.lag.net/downloads/mensch.ttf) (Esta es la que
    yo uso para programar.)

Además he clonado el repo [*Programming
Fonts*](https://github.com/ProgrammingFonts/ProgrammingFonts) y enlazado
algunas fuentes (Hack y Menlo)

    cd ~/wherever
    git clone https://github.com/ProgrammingFonts/ProgrammingFonts
    cd ~/.local/share/fonts
    ln -s ~/wherever/ProgrammingFonts/Hack .
    ln -s ~/wherever/ProgrammingFonts/Menlo .

## Pandoc

*Pandoc* es un traductor entre formatos de documento. Está escrito en
Python y es increiblemente útil. De hecho este documento está escrito
con *Pandoc*.

Instalado el *Pandoc* descargando paquete deb desde [la página web del
proyecto](http://pandoc.org/installing.html).

Además descargamos plantillas adicionales desde [este
repo](https://github.com/jgm/pandoc-templates) ejecutando los siguientes
comandos:

    mkdir ~/.pandoc
    cd ~/.pandoc
    git clone https://github.com/jgm/pandoc-templates templates

## Calibre

La mejor utilidad para gestionar tu colección de libros electrónicos.

Ejecutamos lo que manda la página web:

    sudo -v && wget -nv -O- https://raw.githubusercontent.com/kovidgoyal/calibre/master/setup/linux-installer.py \
    | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"

Para usar el calibre con el Kobo Glo:

  - Desactivamos todos los plugin de Kobo menos el Kobo Touch Extended
  - Creamos una columna MyShelves con identificativo \#myshelves
  - En las opciones del plugin:
      - En la opción Collection columns añadimos las columnas
        series,\#myshelves
      - Marcamos las opciones Create collections y Delete empy
        collections
      - Marcamos *Modify CSS*
      - Update metadata on device y Set series information

Algunos enlaces útiles:

  - (https://github.com/jgoguen/calibre-kobo-driver)
  - (http://www.lectoreselectronicos.com/foro/showthread.php?15116-Manual-de-instalaci%C3%B3n-y-uso-del-plugin-Kobo-Touch-Extended-para-Calibre)
  - (http://www.redelijkheid.com/blog/2013/7/25/kobo-glo-ebook-library-management-with-calibre)
  - (https://www.netogram.com/kobo.htm)

## Scribus

Scribus es un programa libre de composición de documentos. con Scribus
puedes elaborar desde los folletos de una exposición hasta una revista o
un poster.

Para tener una versión más actualizada instalamos:

    sudo add-apt-repository ppa:scribus/ppa
    sudo apt update
    sudo apt install scribus scribus-ng scribus-template scribus-ng-doc

### Cambiados algunos valores por defecto

He cambiado los siguientes valores en las dos versiones, non están
exactamente en el mismo menú pero no son díficiles de encontrar:

  - Lenguaje por defecto: **English**
  - Tamaño de documento: **A4**
  - Unidades por defecto: **milimeters**
  - Show Page Grid: **Activado**
  - Dimensiones de la rejilla:
      - Mayor: **30 mm**
      - Menor: **6mm**
  - En opciones de salida de *pdf* indicamos que queremos salida a
    impresora y no a pantalla. Y también que no queremos *spot colors*,
    que serían sólo para ciertas impresoras industriales, así que
    activamos la opción *Convert Spot Colors to Process Colors*.

Siempre se puede volver a los valores por defecto sin mucho problema
(hay una opción para ello)

Referencia
[aquí](https://www.youtube.com/watch?v=3sEoYZGABQM&list=PL3kOqLpV3a67b13TY3WxYVzErYUOLYekI)

### Solucionados problemas de *hyphenation*

*Scribus* no hacia correctamente la separación silábica en castellano,
he instalado los paquetes:

  - hyphen-es
  - hyphen-gl

Y ahora funciona correctamente.

## Foliate: lector de libros electrónicos

Instalado el paquete deb desde [su propio
github](https://github.com/johnfactotum/foliate/releases)

# Desarrollo software

## Paquetes esenciales

Estos son los paquetes esenciales para empezar a desarrollar software en
Linux.

    sudo apt install build-essential checkinstall make automake cmake autoconf \
    git git-core git-crypt dpkg wget

## Git

-----

**NOTA**: Si quieres instalar la última versión de git, los git
developers tienen un ppa para ubuntu, si quieres tener el git más
actualizado:

``` {bash}
sudo add-apt-repository ppa:git-core/ppa
sudo apt update
sudo apt upgrade
```

-----

Control de versiones distribuido. Imprescindible. Para *Linux Mint*
viene instalado por defecto.

Configuración básica de git:

    git config --global ui.color auto
    git config --global user.name "Pepito Pérez"
    git config --global user.email "pperez@mikasa.com"
    
    git config --global alias.cl clone
    
    git config --global alias.st "status -sb"
    git config --global alias.last "log -1 --stat"
    git config --global alias.lg "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %Cblue<%an>%Creset' --abbrev-commit --date=relative --all"
    git config --global alias.dc "diff --cached"
    
    git config --global alias.unstage "reset HEAD --"
    
    git config --global alias.ci commit
    git config --global alias.ca "commit -a"
    
    git config --global alias.ri "rebase -i"
    git config --global alias.ria "rebase -i --autosquash"
    git config --global alias.fix "commit --fixup"
    git config --global alias.squ "commit --squash"
    
    git config --global alias.cp cherry-pick
    git config --global alias.co checkout
    git config --global alias.br branch
    git config --global core.editor emacs

## Emacs

Instalado emacs desde los repos:

    sudo aptitude install emacs

  - Configuramos la fuente por defecto del editor y salvamos las
    opciones. Con esto generamos el fichero `~/.emacs`
  - **Importante**: Configuramos la *face* para la *region* con un color
    que nos guste. Parece que viene configurado por defecto igual que el
    texto normal y nunca veremos la *region* resaltada aunque queramos.
  - Editamos el fichero `.emacs` y añadimos los depósitos de paquetes
    (nunca he conseguido que *Marmalade* funcione)

Esta es la sección donde configuramos los depósitos de paquetes y que
añadimos a nuestro fichero `~/.emacs`:

    ;;----------------------------------------------------------------------
    ;; MELPA and others
    (when (>= emacs-major-version 24)
      (require 'package)
      (package-initialize)
      (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
      (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
    ;;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)
      )

GNU Elpa es el depósito oficial, tiene menos paquetes y son todos con
licencia FSF.

Melpa y Marmalade son paquetes de terceros. Tienen mucha más variedad
pero con calidades dispares.

Antes de empezar tenemos que instalar el paquete:
*gnu-elpa-keyring-update* por que las claves gpg de GNU Elpa han
cambiado.

Desde el propio *emacs* seguimos los siguientes pasos

  - set package-check-signature to nil, o sea pulsamos `M-: (setq
    package-check-signature nil) RET`
  - Instalamos el paquete `M-x package-install RET
    gnu-elpa-keyring-update RET`
  - Reponemos el valor por defecto de la opción
    *package-check-signature*, es decir *‘allow-unsigned’*

Desde Melpa con el menú de gestión de paquetes de emacs, instalamos los
siguientes paquetes\[2\]:

  - *markdown-mode*
  - *pandoc-mode*
  - *auto-complete*
  - *ac-dcd*
  - *d-mode*
  - *flycheck*
  - *flycheck-dmd-dub*
  - *flycheck-d-unittest*
  - *elpy*
  - *jedi*
  - *auctex-latexmk*
  - *py\_autopep8*
  - *auctex*
  - *smartparens*
  - *yasnippets* (se instala como dependencia)
  - *gnu\_elpa\_keyring\_update* ver más adelante)

Después de probar *flymake* y *flycheck* al final me ha gustado más
*flycheck* Hay una sección de configuración en el fichero `.emacs` para
cada uno de ellos, pero la de *flymake* está comentada.

Mi fichero `~/.emacs`tiene la pinta de lo que viene a continuación. De
momento (si lo copias) dará varios fallos por qué hay que instalar lo
que se detalla en este documento.

    (custom-set-variables
     ;; custom-set-variables was added by Custom.
     ;; If you edit it by hand, you could mess it up, so be careful.
     ;; Your init file should contain only one such instance.
     ;; If there is more than one, they won't work right.
     '(package-check-signature (quote allow-unsigned))
     '(package-selected-packages
       (quote
        (py-autopep8 jedi elpy auctex gnu-elpa-keyring-update smartparens flycheck auto-complete pandoc-mode markdown-mode markdown-mode+))))
    (custom-set-faces
     ;; custom-set-faces was added by Custom.
     ;; If you edit it by hand, you could mess it up, so be careful.
     ;; Your init file should contain only one such instance.
     ;; If there is more than one, they won't work right.
     ;; <salvari> added region face below default font
     '(default ((t (:family "Mensch" :foundry "PfEd" :slant normal :weight normal :height 143 :width normal))))
     '(region ((t (:background "light sea green" :distant-foreground "gtk_selection_fg_color")))))
    
    ;;----------------------------------------------------------------------
    ;; MELPA and others
    (when (>= emacs-major-version 24)
      (require 'package)
      (package-initialize)
      (add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
      (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
    ;;  (add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)
      )
    
    ;;------------------------------------------------------------
    ;; Some settings
    (setq inhibit-startup-message t) ; Eliminate FSF startup msg
    (setq frame-title-format "%b")   ; Put filename in titlebar
    ;(setq visible-bell t)           ; Flash instead of beep
    (set-scroll-bar-mode 'right)     ; Scrollbar placement
    (show-paren-mode t)              ; Blinking cursor shows matching parentheses
    (electric-pair-mode t)           ; electric-pair-mode on
    (setq column-number-mode t)      ; Show column number of current cursor location
    (mouse-wheel-mode t)             ; wheel-mouse support
    (setq fill-column 78)
    (setq auto-fill-mode t)                   ; Set line width to 78 columns...
    (setq-default indent-tabs-mode nil)       ; Insert spaces instead of tabs
    (global-set-key "\r" 'newline-and-indent) ; turn autoindenting on
    ;(set-default 'truncate-lines t)          ; Truncate lines for all buffers
    ;(require 'iso-transl)                    ; doesn't seems to be needed in debian
    (setq sentence-end-double-space nil)
    
    ;;------------------------------------------------------------
    ;; Some useful key definitions
    (define-key global-map [M-S-down-mouse-3] 'imenu)
    (global-set-key [C-tab] 'hippie-expand)                    ; expand
    (global-set-key [C-kp-subtract] 'undo)                     ; [Undo]
    (global-set-key [C-kp-multiply] 'goto-line)                ; goto line
    (global-set-key [C-kp-add] 'toggle-truncate-lines)         ; goto line
    (global-set-key [C-kp-divide] 'delete-trailing-whitespace) ; delete trailing whitespace
    (global-set-key [C-kp-decimal] 'completion-at-point)       ; complete at point
    (global-set-key [C-M-prior] 'next-buffer)                  ; next-buffer
    (global-set-key [C-M-next] 'previous-buffer)               ; previous-buffer
    ;;------------------------------------------------------------
    ;; Set encoding
    (prefer-coding-system 'utf-8)
    ;;(setq coding-system-for-read 'utf-8)
    ;;(setq coding-system-for-write 'utf-8)
    
    ;; UTF-8 please
    ;;(setq locale-coding-system 'utf-8)   ; pretty
    ;;(set-terminal-coding-system 'utf-8)  ; pretty
    ;;(set-keyboard-coding-system 'utf-8)  ; pretty
    ;;(set-selection-coding-system 'utf-8) ; please
    ;;(setq-default buffer-file-coding-system 'utf-8-unix)
    ;;(setq-default default-buffer-file-coding-system 'utf-8-unix)
    ;;(set-default-coding-systems 'utf-8-unix)
    ;;(prefer-coding-system 'utf-8-unix)
    ;;(when (eq system-type 'windows-nt)
    ;;  (set-clipboard-coding-system 'utf-16le-dos))
    ;;------------------------------------------------------------
    ;; Maximum colors
    (cond ((fboundp 'global-font-lock-mode) ; Turn on font-lock (syntax highlighting)
    (global-font-lock-mode t)               ; in all modes that support it
    (setq font-lock-maximum-decoration t))) ; Maximum colors
    ;;------------------------------------------------------------
    ;; Use % to match various kinds of brackets...
    ;; See: http://www.lifl.fr/~hodique/uploads/Perso/patches.el
    (global-set-key "%" 'match-paren)   ; % key match parents
    (defun match-paren (arg)
    "Go to the matching paren if on a paren; otherwise insert %."
    (interactive "p")
    (let ((prev-char (char-to-string (preceding-char)))
    (next-char (char-to-string (following-char))))
    (cond ((string-match "[[{(<]" next-char) (forward-sexp 1))
    ((string-match "[\]})>]" prev-char) (backward-sexp 1))
    (t (self-insert-command (or arg 1))))))
    
    ;;------------------------------------------------------------
    ;; The wonderful bubble-buffer
    (defvar LIMIT 1)
    (defvar time 0)
    (defvar mylist nil)
    (defun time-now ()
    (car (cdr (current-time))))
    (defun bubble-buffer ()
    (interactive)
    (if (or (> (- (time-now) time) LIMIT) (null mylist))
        (progn (setq mylist (copy-alist (buffer-list)))
           (delq (get-buffer " *Minibuf-0*") mylist)
           (delq (get-buffer " *Minibuf-1*") mylist)))
    (bury-buffer (car mylist))
    (setq mylist (cdr mylist))
    (setq newtop (car mylist))
    (switch-to-buffer (car mylist))
    (setq rest (cdr (copy-alist mylist)))
    (while rest
      (bury-buffer (car rest))
      (setq rest (cdr rest)))
    (setq time (time-now)))
    (global-set-key [f8] 'bubble-buffer)
    ; win-tab switch the buffer
    (defun geosoft-kill-buffer ()
      ;; Kill default buffer without the extra emacs questions
      (interactive)
      (kill-buffer (buffer-name))
      (set-name))
    (global-set-key [C-delete] 'geosoft-kill-buffer)
    
    ; (add-to-list 'load-path "~/.emacs.d/")
    
    ;;----------------------------------------------------------------------
    ;; Packages installed via package
    ;;------------------------------
    
    ;;----------------------------------------------------------------------
    ;; flymake and flycheck installed from package
    ;; I think you have to choose only one
    
    ;; (require 'flymake)
    ;; ;;(global-set-key (kbd "C-c d") 'flymake-display-err-menu-for-current-line)
    ;; (global-set-key (kbd "C-c d") 'flymake-popup-current-error-menu)
    ;; (global-set-key (kbd "C-c n") 'flymake-goto-next-error)
    ;; (global-set-key (kbd "C-c p") 'flymake-goto-prev-error)
    
    (add-hook 'after-init-hook #'global-flycheck-mode)
    (global-set-key  (kbd "C-c C-p") 'flycheck-previous-error)
    (global-set-key  (kbd "C-c C-n") 'flycheck-next-error)
    
    ;; Define d-mode addons
    ;; Activate flymake or flycheck for D
    ;; Activate auto-complete-mode
    ;; Activate yasnippet minor mode if available
    ;; Activate dcd-server
    (require 'ac-dcd)
    (add-hook 'd-mode-hook
              (lambda()
                ;;(flymake-d-load)
                (flycheck-dmd-dub-set-variables)
                (require 'flycheck-d-unittest)
                (setup-flycheck-d-unittest)
                (auto-complete-mode t)
                (when (featurep 'yasnippet)
                  (yas-minor-mode-on))
                (ac-dcd-maybe-start-server)
                (ac-dcd-add-imports)
                (add-to-list 'ac-sources 'ac-source-dcd)
                (define-key d-mode-map (kbd "C-c ?") 'ac-dcd-show-ddoc-with-buffer)
                (define-key d-mode-map (kbd "C-c .") 'ac-dcd-goto-definition)
                (define-key d-mode-map (kbd "C-c ,") 'ac-dcd-goto-def-pop-marker)
                (define-key d-mode-map (kbd "C-c s") 'ac-dcd-search-symbol)
                (when (featurep 'popwin)
                  (add-to-list 'popwin:special-display-config
                               `(,ac-dcd-error-buffer-name :noselect t))
                  (add-to-list 'popwin:special-display-config
                               `(,ac-dcd-document-buffer-name :position right :width 80))
                  (add-to-list 'popwin:special-display-config
                               `(,ac-dcd-search-symbol-buffer-name :position bottom :width 5)))))
    
    ;; Define diet template mode (this is not installed from package)
    (add-to-list 'auto-mode-alist '("\\.dt$" . whitespace-mode))
    (add-hook 'whitespace-mode-hook
              (lambda()
                (setq tab-width 2)
                (setq whitespace-line-column 250)
                (setq indent-tabs-mode nil)
                (setq indent-line-function 'insert-tab)))
    
    ;;----------------------------------------------------------------------
    ;; python
    (elpy-enable)
    
    ;; Enable Flycheck
    
    (when (require 'flycheck nil t)
    
      (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    
      (add-hook 'elpy-mode-hook 'flycheck-mode))
    
    ;; Enable autopep8, see alternative: black
    
    (require 'py-autopep8)
    
    (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)
    
    
    ;; enable jedi for elpy
    (setq elpy-rpc-backend "jedi")
    
    (add-hook 'python-mode-hook 'jedi:setup)
    (setq jedi:complete-on-dot t)
    
    
    (provide '.emacs)
    ;;; .emacs ends here

## Lenguaje de programación D (D programming language)

El lenguaje de programación D es un lenguaje de programación de sistemas
con una sintaxis similar a la de C y con tipado estático. Combina
eficiencia, control y potencia de modelado con seguridad y
productividad.

### D-apt e instalación de programas

Configurado *d-apt*, instalados todos los programas incluidos

    sudo wget http://master.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  EBCF975E5BA24D5E
    sudo apt update

Instalamos todos los programas asociados excepto *textadept* que falla
por problemas de librerias.

    sudo apt install dmd-compiler dmd-tools dub dcd dfix dfmt dscanner

### DCD

Una vez instalado el DCD tenemos que configurarlo creando el fichero
`~/.config/dcd/dcd.conf` con el siguiente contenido:

    /usr/include/dmd/druntime/import
    /usr/include/dmd/phobos

Podemos probarlo con:

    dcd-server &
    echo | dcd-client --search toImpl

### gdc

Instalado con:

    sudo aptitude install gdc

### ldc

Instalado con:

    sudo aptitude install ldc

Para poder ejecutar aplicaciones basadas en Vibed, necesitamos instalar:

    sudo apt-get install -y libssl-dev libevent-dev

### Emacs para editar D

Instalados los siguientes paquetes desde Melpa

  - d-mode
  - flymake-d
  - flycheck
  - flycheck-dmd-dub
  - flychek-d-unittest
  - auto-complete (desde melpa)
  - ac-dcd

Referencias \* (https://github.com/atilaneves/ac-dcd) \*
(https://github.com/Hackerpilot/DCD)

## C, C++

### Instalación de Gnu Global

Para instalar las dependencias, previamente instalamos:

``` {shell}
sudo apt install ncurses-dev id-utils exuberant-ctags python-pygments
```

Con `ctags --version` nos aseguramos de que se llama a Exuberant y no el
ctags que instala Emacs. Si no es así habrá que revisar la definición
del `PATH`

`python-pygments` no es necesario para C o C++, pero añade funcionalidad
a Global (hasta 25 lenguajes de programación más)

No podemos instalar Global desde los repos de Ubuntu, está muy anticuado
y genera bases de datos enormes y lentas. Tendremos que compilarlo.

Nos bajamos las fuentes del programa desde [la página
oficial](https://www.gnu.org/software/global/) En el momento de escribir
esto se trata de la versión 6.6.4.

Descomprimimos los fuentes y los compilamos con:

``` {shell}
./configure --prefix=/usr/local --with-exuberant-ctags=/usr/bin/ctags
make
sudo make install
```

He comprobado que make uninstall funciona correctamente, las librerías
quedan instaladas en `/usr/local/lib/gtags` y los ejecutables en
`/usr/local/bin`

## Processing

Bajamos los paquetes de las respectivas páginas web, descomprimimimos en
`~/apps/`, en las nuevas versiones incorpora un script de instalación
que ya se encarga de crear el fichero *desktop*.

La última versión incorpora varios modos de trabajo, he descargado el
modo *Python* para probarlo.

## openFrameworks

Nos bajamos los fuentes para linux 64bits desde [la página web del
proyecto](https://openframeworks.cc), y las descomprimimos en un
directorio para proceder a compilarlas.

No hay más que seguir [las instrucciones de instalación para
linux](https://openframeworks.cc/setup/linux-install/).

La instalación no es demasiado intrusiva si tienes Ubuntu 18 o mayor y
una versión reciente del gcc.

En la primera pregunta que nos hace es necesario contestar que no. De lo
contrario falla la compilación.

Añade los siguientes paquetes a nuestro sistema

    libglfw3-dev
    
    
    curl libjack-jackd2-0 libjack-jackd2-dev freeglut3-dev libasound2-dev libxmu-dev libxxf86vm-dev g++ libgl1-mesa-dev-hwe-18.04 libglu1-mesa-dev libraw1394-dev libudev-dev libdrm-dev libglew-dev libopenal-dev libsndfile-dev libfreeimage-dev libcairo2-dev libfreetype6-dev libssl-dev libpulse-dev libusb-1.0-0-dev libgtk-3-dev libopencv-dev libassimp-dev librtaudio-dev libboost-filesystem-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-libav gstreamer1.0-pulseaudio gstreamer1.0-x gstreamer1.0-plugins-bad gstreamer1.0-alsa gstreamer1.0-plugins-base gstreamer1.0-plugins-good gdb libglfw3-dev liburiparser-dev libcurl4-openssl-dev libpugixml-dev libgconf-2-4 libgtk2.0-0 libpoco-dev

No te olvides de compilar también el *Project Generator*.

## Python

De partida tenemos instalado dos versiones: *python* y *python3*

``` {bash}
python -V
Python 2.7.12

python3 -V
Python 3.5.2
```

### Paquetes de desarrollo

Para que no haya problemas a la hora de instalar paquetes en el futuro
conviene que instalemos los paquetes de desarrollo:

    sudo apt install python-dev
    sudo apt install python3-dev

### pip, virtualenv, virtualenvwrapper, virtualfish

Los he instalado a nivel de sistema.

*pip* es un gestor de paquetes para **Python** que facilita la
instalación de librerías y utilidades.

Para poder usar los entornos virtuales instalaremos también
*virtualenv*.

Instalamos los dos desde aptitude:

``` {bash}
sudo apt install python-pip python-virtualenv virtualenv python3-pip
```

*virtualenv* es una herramienta imprescindible en Python, pero da un
poco de trabajo, así que se han desarrollado algunos frontends para
simplificar su uso, para *bash* y *zsh* usaremos *virtualenvwrapper*, y
para *fish* el *virtualfish*. Como veremos son todos muy parecidos.

Instalamos el virtualwrapper:

``` {bash}
sudo apt install virtualenvwrapper -y
```

Para usar *virtualenvwrapper* tenemos que hacer:

``` {bash}
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
```

O añadir esa linea a nuestros ficheros *.bashrc* y/o *.zshrc*

Definimos la variable de entorno *WORKON\_HOME* para que apunte al
directorio por defecto, `~/.local/share/virtualenvs`. En ese directorio
es donde se guardarán nuestros entornos virtuales.

En el fichero `.profile` añadimos:

    # WORKON_HOME for virtualenvwrapper
    if [ -d "$HOME/.local/share/virtualenvs" ] ; then
        WORKON_HOME="$HOME/.local/share/virtualenvs"
    fi

[Aquí](http://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html)
tenemos la referencia de comandos de *virtualenvwrapper*

Por último, si queremos tener utilidades parecidas en nuestro *fish
shell* instalamos *virtualfish*:

``` {bash}
sudo pip install virtualfish
```

[Aquí](http://virtualfish.readthedocs.io/en/latest/index.html) tenemos
la documentación de *virtualfish* y la descripción de todos los comandos
y plugins disponibles.

### pipenv

No lo he instalado, pero en caso de instalación mejor lo instalamos a
nivel de usuario con:

``` {bash}
pip install --user pipenv
```

### Instalación del Python 3.8 (última disponible)

Ejecutamos:

``` {bash}
sudo apt install python3.8 python3.8-dev python3.8-venv
```

### Instalación de bpython y ptpython

[*bpython*](https://bpython-interpreter.org/) instalado desde repos
`sudo apt install bpython bpython3`

[*ptpython*](https://github.com/prompt-toolkit/ptpython) instalado en un
virtualenv para probarlo

### Emacs para programar python

#### elpy: Emacs Python Development Enviroment

Para instalar `elpy` necesitamos intalar previamente *venv* el nuevo
gestor de *virtualenvs* en Python 3.:

    sudo apt install python3-venv

En el fichero `~/.emacs` necesitamos activar el módulo *elpy*:

``` {lisp}
;;----------------------------------------------------------------------
;; elpy
(elpy-enable)
```

En cuanto activemos *elpy* tendremos autocompletado del código y errores
sintácticos. Merece la pena leerse toda la
[documentación](https://elpy.readthedocs.io/en/latest/)

#### Flycheck

Para tener análisis sintáctico en tiempo real mientras estamos
programando:

Añadimos a nuestro fichero `~/.emacs`:

    ;; Enable Flycheck
    
    (when (require 'flycheck nil t)
    
      (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    
      (add-hook 'elpy-mode-hook 'flycheck-mode))

#### Formateado

Usando *autopep8* o *black* tendremos autoformateado del código como
paso previo a salvar el mismo en disco. (Yo aún no he probado *black*)

    # and autopep8 for automatic PEP8 formatting
    sudo apt install python-autopep8
    # and yapf for code formatting (innecesario)
    # sudo apt install yapf yapf3

Y añadimos la sección siguiente a nuestro fichero `~/.emacs`

``` {lisp}
;; Enable autopep8

(require 'py-autopep8)

(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)
```

#### jedi

Jedi le da ciertos superpoderes al autocompletado visualizando la
documentación de cada propuesta de autocompletado.

Instalamos previamente:

``` {bash}
sudo apt install python-jedi python3-jedi
# flake8 for code checks
sudo apt install flake8 python-flake8 python3-flake8
```

Y añadimos la sección en el fichero `~/.emacs`:

``` {lisp}
;;----------------------------------------------------------------------
;; elpy
(elpy-enable)
(setq elpy-rpc-backend "jedi")

(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)
```

Desde *Emacs* ejecutamos: `alt-x jedi:install-server`

### Jupyter

Una instalación para pruebas.

    mkvirtualenv -p /usr/bin/python3 jupyter
    python -m pip install jupyter

## neovim

Vamos a probar *neovim*:

    sudo apt-add-repository ppa:neovim-ppa/stable
    sudo apt update
    sudo apt install neovim

Para instalar los módulos de python creamos un *virtualev* que más tarde
añadiremos al fichero `init.vim`.

    mkvirtualenv -p /usr/bin/python3 neovim3
    sudo pip install --upgrade neovim
    deactivate

Revisar [esto](https://neovim.io/doc/user/provider.html#provider-python)

|                                                                      |
| :------------------------------------------------------------------- |
| **NOTA**: El siguiente paso ya no parece necesario, las alternativas |
| se han actualizado con la instalación del *neovim*.                  |

Para actualizar las alternativas:

    sudo update-alternatives --install /usr/bin/vi vi /usr/bin/nvim 60
    sudo update-alternatives --config vi
    sudo update-alternatives --install /usr/bin/vim vim /usr/bin/nvim 60
    sudo update-alternatives --config vim

#### Install *vim-plug*

Ejecutamos:

    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

Configuramos el fichero de configuración de *nvim*
(`~/.config/nvim/init.vim`):

``` 
" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.local/share/nvim/plugged')

if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'deoplete-plugins/deoplete-jedi'

" Initialize plugin system
call plug#end()

let g:deoplete#enable_at_startup = 1

" set python enviroments
let g:python_host_prog = '/full/path/to/neovim2/bin/python'
let g:python3_host_prog = '/home/salvari/.virtualenvs/neovim3/bin/python'

```

La primera vez que abramos *nvim* tenemos que instalar los plugin porn
comando ejecutando: `:PlugInstall`

**Instalación de `dein`**

|                                                                    |
| :----------------------------------------------------------------- |
| **Nota**:                                                          |
| Solo hay que instalar uno de los dos o *dein* o *plug-vim*. Yo uso |
| *plug-vim* así que esto es sólo una referencia.                    |

<https://github.com/Shougo/dein.vim>

    " Add the dein installation directory into runtimepath
    set runtimepath+=~/.config/nvim/dein/repos/github.com/Shougo/dein.vim
    
    if dein#load_state('~/.config/nvim/dein')
      call dein#begin('~/.config/nvim/dein')
    
      call dein#add('~/.config/nvim/dein/repos/github.com/Shougo/dein.vim')
      call dein#add('Shougo/deoplete.nvim')
      call dein#add('Shougo/denite.nvim')
      if !has('nvim')
        call dein#add('roxma/nvim-yarp')
        call dein#add('roxma/vim-hug-neovim-rpc')
      endif
    
      call dein#end()
      call dein#save_state()
    endif
    
    filetype plugin indent on
    syntax enable

## Firefox developer edition

El rollo de siempre, descargar desde [la página
web](https://www.mozilla.org/en-US/firefox/developer/) descomprimir en
`~/apps` y crear un lanzador.

## Navegadores cli

Herramientas útiles para depuración web

``` {bash}
sudo apt install httpie links
```

## MariaDB

Instalamos la última estable para Ubuntu Bionic desde los repos
oficiales.

Primero añadimos los reports

Añadimos la clave de firma:

``` {bash}
sudo apt-get install software-properties-common
sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'
```

Ahora tenemos dos opciones:

Podemos ejecutar:

    sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://ftp.icm.edu.pl/pub/unix/database/mariadb/repo/10.4/ubuntu bionic main'

O podemos crear un fichero `/etc/apt/apt.sources.list.d/MariaDB` con el
siguiente contenido (yo dejo las fuentes comentadas):

    # MariaDB 10.4 repository list - created 2020-01-26 10:37 UTC
    # http://downloads.mariadb.org/mariadb/repositories/
    deb [arch=amd64,arm64,ppc64el] http://ftp.ubuntu-tw.org/mirror/mariadb/repo/10.4/ubuntu bionic main
    # deb-src http://ftp.ubuntu-tw.org/mirror/mariadb/repo/10.4/ubuntu bionic main

Y ya solo nos queda lo de siempre:

    sudo apt update
    sudo apt upgrade
    sudo apt install mariadb-server

Podemos comprobar con `systemctl status mariadb`

También podemos hacer login con el usuario `root`:

    sudo mariadb -u root

Y ahora aseguramos la instación con:

    sudo mysql_secure_installation

Yo diría que tienes que decir que si a todas las preguntas, excepto
quizás al *unix\_socket\_authentication*.

Por último sólo nos queda decidir si el servicio mariadb debe estar
ejecutándose permanentemente o no.

Si queremos pararlo y que no se arranque automáticamente al arrancar el
ordenador:

    sudo systemctl stop mariadb
    sudo systemctl disable mariadb

## Squirrel SQL Client

Bajamos el zip de estándar desde [la página web de
Squirrel](http://www.squirrelsql.org/) (yo prefiero no usar el
instalador)

Como de costumbre descomprimimos en `~/apps` y creamos una entrada en
nuestro menú de aplicaciones.

Nos descargamos también el *java connector* para MariaDB. Desde la
página oficial. Nos interesa el fichero `maria-java-client-2.6.0.jar`

Configuramos el driver para que sepa donde está el fichero `.jar` y ya
estamos listos para trabajar.

## R y R-studio

Primero instalamos la última versión de R en nuestro pc:

``` {bash}
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/'
sudo apt install r-base
```

### R-studio

Descargamos la última versión disponible de *R-studio* desde la [página
web](https://cloud.r-project.org/bin/linux/ubuntu)

Instalamos con *gdebi* (basta con clicar sobre el fichero *.deb*)

## Octave

Instalado desde flatpak

    sudo flatpak install flathub org.octave.Octave

# Desarrollo hardware

## Arduino IDE

Bajamos los paquetes de la página [web](https://www.arduino.cc),
descomprimimimos en *\~/apps/arduino*.

La distribución del IDE incluye ahora un fichero `install.sh`que se
encarga de hacer la integración del IDE en los menús de Linux.

Además también incluye un script (`arduino-linux-setup.sh`) para crear
las *devrules* y que además desinstala el driver *modemmanager* y crea
grupos nuevos en el sistema si no existen.

No tengo claro lo de desinstalar el driver así que creamos las
*devrules* a mano mirando por el fichero.

Hay que añadir nuestro usuario a los grupos *tty*, *dialout*, *uucp* y
*plugdev* (no hay que crear grupos nuevos, ya tenemos todos en el
sistema)

    sudo gpasswd --add <usrname> tty
    sudo gpasswd --add <usrname> dialout
    sudo gpasswd --add <usrname> uucp
    sudo gpasswd --add <usrname> plugdev

Creamos los siguientes ficheros en el directorio `/etc/udev/rules.d`

Fichero `90-extraacl.rules` mete mi usario en el fichero de reglas
(¬\_¬)

    # Setting serial port rules
    
    KERNEL=="ttyUSB[0-9]*", TAG+="udev-acl", TAG+="uaccess", OWNER="salvari"
    KERNEL=="ttyACM[0-9]*", TAG+="udev-acl", TAG+="uaccess", OWNER="salvari"

Fichero `98-openocd.rules`

    # Adding Arduino M0/M0 Pro, Primo UDEV Rules for CMSIS-DAP port
    
    ACTION!="add|change", GOTO="openocd_rules_end"
    SUBSYSTEM!="usb|tty|hidraw", GOTO="openocd_rules_end"
    
    #Please keep this list sorted by VID:PID
    
    #CMSIS-DAP compatible adapters
    ATTRS{product}=="*CMSIS-DAP*", MODE="664", GROUP="plugdev"
    
    LABEL="openocd_rules_end"

Fichero `avrisp.rules`

    # Adding AVRisp UDEV rules
    
    SUBSYSTEM!="usb_device", ACTION!="add", GOTO="avrisp_end"
    # Atmel Corp. JTAG ICE mkII
    ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2103", MODE="660", GROUP="dialout"
    # Atmel Corp. AVRISP mkII
    ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2104", MODE="660", GROUP="dialout"
    # Atmel Corp. Dragon
    ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2107", MODE="660", GROUP="dialout"
    
    LABEL="avrisp_end"

Fichero `40-defuse.rules`:

    # Adding STM32 bootloader mode UDEV rules
    
    # Example udev rules (usually placed in /etc/udev/rules.d)
    # Makes STM32 DfuSe device writeable for the "plugdev" group
    
    ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="664", GROUP="plugdev", TAG+="uaccess"

Fichero `99-arduino-101.rules`:

    # Arduino 101 in DFU Mode
    
    SUBSYSTEM=="tty", ENV{ID_REVISION}=="8087", ENV{ID_MODEL_ID}=="0ab6", MODE="0666", ENV{ID_MM_DEVICE_IGNORE}="1", ENV{ID_MM_CANDIDATE}="0"
    SUBSYSTEM=="usb", ATTR{idVendor}=="8087", ATTR{idProduct}=="0aba", MODE="0666", ENV{ID_MM_DEVICE_IGNORE}="1"

Yo añado el fichero `99-arduino.rules` que se encarga de inhibir el
modemmanager para que no capture al *CircuitPlayground Express*:

    # for arduino brand, stop ModemManager grabbing port
    ATTRS{idVendor}=="2a03", ENV{ID_MM_DEVICE_IGNORE}="1"
    # for sparkfun brand, stop ModemManager grabbing port
    ATTRS{idVendor}=="1b4f", ENV{ID_MM_DEVICE_IGNORE}="1"

### Añadir soporte para *Feather M0*

Arrancamos el IDE Arduino y en la opción de *Preferences::Aditional Boar
Managers URLs* añadimos la dirección
`https://adafruit.github.io/arduino-board-index/package_adafruit_index.json`,
si tenemos otras URL, simplemente añadimos esta separada por una coma.

Ahora desde el *Board Manager* instalamos:

  - Arduino SAMD Boards
  - Adafruit SAMD Boards

### Añadir soporte para *Circuit Playground Express*

Bastaría con instalar *Arduino SAMD Boards*

### Añadir soporte para STM32

Tenemos varias URL posibles para configurar en las preferencias del IDE
Arduino:

  - <http://dan.drown.org/stm32duino/package_STM32duino_index.json>
    (recomendada por Tutoelectro)
  - <https://github.com/stm32duino/BoardManagerFiles/raw/master/STM32/package_stm_index.json>
    (parece la oficial, y tiene mejor pinta)

### Añadir soporte para ESP32

### Añadir biblioteca de soporte para Makeblock

|                                 |
| :------------------------------ |
| **Nota**: Pendiente de instalar |

Clonamos el [repo oficial en
github](https://github.com/Makeblock-official/Makeblock-Libraries).

Una vez que descarguemos las librerias es necesario copiar el directorio
`Makeblock-Libraries/makeblock` en nuestro directorio de bibliotecas de
Arduino. En mi caso `~/Arduino/libraries/`.

Una vez instaladas las bibliotecas es necesario reiniciar el IDE Arduino
si estaba arrancado. Podemos ver si se ha instalado correctamente
simplemente echando un ojo al menú de ejemplos en el IDE, tendríamos que
ver los ejemplos de *Makeblock*.

Un detalle importante para programar el Auriga-Me es necesario
seleccionar el micro Arduino Mega 2560 en el IDE Arduino.

## Pinguino IDE

|                                 |
| :------------------------------ |
| **Nota**: Pendiente de instalar |

Tenemos el paquete de instalación disponible en su página
[web](http://pinguino.cc/download.php)

Ejecutamos el programa de instalación. El programa descargará los
paquetes Debian necesarios para dejar el IDE y los compiladores
instalados.

Al acabar la instalación he tenido que crear el directorio
*\~/Pinguino/v11*, parece que hay algún problema con el programa de
instalación y no lo crea automáticamente.

El programa queda correctamente instalado en */opt* y arranca
correctamente, habrá que probarlo con los micros.

## esp-idf

Instalamos las dependencias (cmake ya lo tenemos instalado)

|                                                                   |
| :---------------------------------------------------------------- |
| **NOTA**: No es necesario instalar los paquetes de python que nos |
| especifican en las instrucciones de instalación del *esp-idf*, se |
| instalarán automáticamente en el siguiente paso.                  |

``` {bash}
sudo apt-get install gperf  cmake ninja-build ccache libffi-dev libssl-dev
```

Ahora creamos un directorio para nuestro *tool-chain*:

``` {bash}
mkdir ~/esp
cd ~/esp
git clone --recursive https://github.com/espressif/esp-idf
```

También es necesario que nuestro usuario pertenezca al grupo `dialout`,
pero eso ya deberíamos tenerlo hecho de antes.

Una vez clonado el repo ejecutamos el script de instalación

``` {bash}
cd ~/esp/esp-idf
./install.sh
```

Este script nos va a dejar instaladas todas las herramientas necesarias
en el directorio `~/.expressif`

Para empezar a trabajar bastará con hacer un *source* del fichero
`~/esp/esp-idf/export.sh`:

``` {bash}
. ~/esp/esp-idf/export.sh
```

## KiCAD

En la [página web del
proyecto](http://kicad-pcb.org/download/linux-mint/) nos recomiendan el
ppa a usar para instalar la última versión estable:

    sudo add-apt-repository --yes ppa:js-reynaud/kicad-5
    sudo apt-get update
    sudo apt-get install kicad
    sudo apt install kicad-footprints kicad-libraries kicad-packages3d kicad-symbols kicad-templates

Paciencia, el paquete `kicad-packages3d` tarda un buen rato en
descargarse.

Algunas librerías alternativas:

  - [Freetronics](https://github.com/freetronics/freetronics_kicad_library)
    una libreria que no solo incluye Shield para Arduino sino una
    completa colección de componentes que nos permitirá hacer proyectos
    completos. [Freetronics](http://www.freetronics.com) es una especie
    de BricoGeek australiano, publica tutoriales, vende componentes, y
    al parecer mantiene una biblioteca para KiCAD. La biblioteca de
    Freetronics se mantiene en un repo de github. Lo suyo es
    incorporarla a cada proyecto, por que si la actualizas se pueden
    romper los proyectos que estes haciendo.
  - [eklablog](http://meta-blog.eklablog.com/kicad-librairie-arduino-pretty-p930786)
    Esta biblioteca de componentes está incluida en el github de KiCAD,
    así que teoricamente no habría que instalarla en nuestro disco duro.

## Analizador lógico

Mi analizador es un OpenBench de Seedstudio, [aquí hay mas
info](http://dangerousprototypes.com/docs/Open_Bench_Logic_Sniffer)

### Sigrok

Instalamos **Sigrok**, simplemente desde los repos de Debian:

``` {bash}
sudo aptitude install sigrok
```

Al instalar **Sigrok** instalamos también **Pulseview**.

Si al conectar el analizador, echamos un ojo al fichero *syslog* vemos
que al conectarlo se mapea en un puerto tty.

Si arrancamos **Pulseview** (nuestro usuario tiene que estar incluido en
el grupo *dialout*), en la opción *File::Connect to device*, escogemos
la opción *Openbench* y le pasamos el puerto. Al pulsar la opción *Scan
for devices* reconoce el analizador correctamente como un *Sump Logic
Analyzer*.

### Sump logic analyzer

Este es el software recomendado para usar con el analizador.

Descargamos el paquete de la [página del
proyecto](https://www.sump.org), o más concretamente de [esta
página](https://www.sump.org/projects/analyzer/) y descomprimimos en
*\~/apps*.

Instalamos las dependencias:

``` {bash}
sudo apt install librxtx-java
```

Editamos el fichero *\~/apps/Logic Analyzer/client/run.sh* y lo dejamos
así:

    #!/bin/bash
    
    # java -jar analyzer.jar $*
    java -cp /usr/share/java/RXTXcomm.jar:analyzer.jar org.sump.analyzer.Loader

Y ya funciona.

### OLS

|                                 |
| :------------------------------ |
| **Nota**: Pendiente de instalar |

[Página oficial](https://www.lxtreme.nl/ols/)

## IceStudio

Instalamos dependencias con `sudo apt install xclip`

Bajamos el *AppImage* desde el [github de
IceStudio](https://github.com/FPGAwars/icestudio) y lo dejamos en
`~/apps/icestudio`

## PlatformIO

### VS Code

Añadimos el origen de software:

    curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
    sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
    sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

E instalamos

    sudo apt update
    sudo apt install code   # alternativamente code-insiders (es como la versión beta, se pueden instalar los dos)

Ahora

1.  lazamos el editor
2.  abrimos el gestor de extensiones
3.  buscamos el platformio ide
4.  instalamos

Seguimos las instrucciones de
[aqui](https://docs.platformio.org/en/latest/ide/vscode.html#quick-start)

### Incluir platform.io CLI en el PATH

Modificamos el fichero `~/.profile` añadiendo las siguientes lineas:

    if [ -d "$HOME/.platformio/penv/bin"] ; then
        PATH="$PATH:$HOME/.platformio/penv/bin"
    fi

### Editor Atom

|                                                                       |
| :-------------------------------------------------------------------- |
| *NOTA*: Parece que antes recomendaban instalar Atom para disponer del |
| Platformio CLI, ahora en cambio recomiendan VS Code.                  |

    wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
    sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
    sudo apt update
    sudo apt install atom

Completamos la instalación del paquete *Platformio IDE* desde el Atom
siguiendo las instrucciones
[aquí](https://platformio.org/get-started/ide?install=atom).

Para poder usar *platformio* sin usar *Atom* añadimos la siguiente linea
al fichero `.profile`:

    export PATH=$PATH:~/.platformio/penv/bin

  - [Referencia](https://docs.platformio.org/en/latest/installation.html#piocore-install-shell-commands)

## RepRap

### OpenScad

El OpenSCAD *no está* disponible en los orígenes de software, es
necesario añadir un ppa:

    sudo add-apt-repository ppa:openscad/releases
    sudo apt update
    sudo apt install openscad

### Slic3r

Descargamos la estable desde la [página web](https://dl.slic3r.org) y
como de costumbre descomprimimos en `~/apps` y creamos un lanzador con
*MenuLibre*

### Slic3r Prusa Edition

Una nueva versión del clásico *Slic3r* con muchas mejoras. Descargamos
la *appimage* desde la [página
web](https://www.prusa3d.com/slic3r-prusa-edition/) y ya sabeis,
descomprimir en `~/apps` y dar permisos de ejecución.

### ideaMaker

Una aplicación más para generar gcode con muy buena pinta, tenemos el
paquete *deb* disponible en su [página
web](https://www.raise3d.com/pages/ideamaker). Instalamos con el gestor
de software.

### Ultimaker Cura

Descargamos el *AppImage* desde la [página
web](https://github.com/Ultimaker/Cura/releases)

### Pronterface

Seguimos las instrucciones para Ubuntu Bionic:

Instalamos las dependencias:

Clonamos el repo:

    cd ~/apps
    git clone https://github.com/kliment/Printrun.git
    cd Printrun
    mkvirtualenv -p /usr/bin/python3 printrun
    python -m pip install https://extras.wxpython.org/wxPython4/extras/linux/gtk2/ubuntu-18.04/wxPython-4.0.7-cp36-cp36m-linux_x86_64.whl
    
    sudo apt-get install libdbus-glib-1-dev libdbus-1-dev

Y ya lo tenemos todo listo para ejecutar.

## Cortadora de vinilos

### Inkcut

Instalado en un entorno virtual:

``` {bash}
mkvirtualenv -p `which python3` inkcut

sudo apt install libxml2-dev libxslt-dev libcups2-dev

pip install PyQt5

pip install inkcut
```

### Plugin para inkscape

Instalamos dependencias:

``` {bash}
pip install python-usb
```

Instalamos el fichero `.deb` desde la web
<https://github.com/fablabnbg/inkscape-silhouette/releases>

# Aplicaciones de gráficos

## LibreCAD

Diseño en 2D

    sudo apt install librecad

## FreeCAD

Añadimos el ppa de la última estable:

    sudo add-apt-repository ppa:freecad-maintainers/freecad-stable
    sudo apt update
    sudo install freecad

|                                                              |
| :----------------------------------------------------------- |
| **NOTA:** the ccx package brings CalculiX support to the FEM |
| workbench, and needs to be installed separately.             |

## Inkscape

El programa libre para creación y edición de gráficos vectoriales.

    sudo apt install inkscape

## Gimp

El programa para edición y retocado de imágenes.

Gimp ya estaba instalado, pero no es la última versión, prefiero tener
la última así que:

    sudo apt remove gimp gimp-data
    sudo add-apt-repository ppa:otto-kesselgulasch/gimp
    sudo apt update
    sudo apt upgrade
    sudo apt install gimp gimp-data gimp-texturize \
    gimp-data-extras gimp-gap gmic gimp-gmic gimp-python

### Plugins de Gimp

Para instalar los principales plugins basta con:

    sudo apt install gimp-plugin-registry

|                                                                                                                                            |
| :----------------------------------------------------------------------------------------------------------------------------------------- |
| Esta sección ya no está vigente                                                                                                            |
| \#\#\#\# resynthesizer                                                                                                                     |
| Descargamos el plugin desde [aquí](https://github.com/bootchk/resynthesizer) y descomprimimos el fichero en `~/.config/GIMP/2.10/plug-ins` |
| Tenemos que asegurarnos que los fichero *python* son ejecutables:                                                                          |
| \~\~\~\~ chmod 755 \~/.config/GIMP/2.10/plug-ins/\*.py \~\~\~\~                                                                            |

## Krita

La versión disponible en orígenes de software está bastante por detrás
de la disponible en la web. Basta con descargar el *Appimage* desde la
[página web](https://krita.org)

Lo copiamos a `~/apps/krita` y creamos un lanzador con el editor de
menús.

Alternativamente también lo tenemos disponible por ppa en
<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>

## MyPaint

Desde el [github](https://github.com/mypaint/) tenemos disponible la
última versión en formato *appimage*. La descargamos la dejamos en
`~/apps` y creamos un acceso con *Menulibre*, como siempre.

## Alchemy

Igual que el *MyPaint* descargamos desde [la página
web](http://al.chemy.org), descomprimimos en `~/apps` y creamos un accso
con *Menulibre*.

## Capturas de pantalla

Resulta que *Shutter* ya no está disponible. Aunque hay algún método
para instalarlo he preferido probar las alternativas *flameshot* y
*knips*.

El [*flameshot*](https://flameshot.js.org/#/) cubre el 99% de mis
necesidades: `sudo apt install flameshot`

El [*ksnip*](https://github.com/DamirPorobic/ksnip) por si tenemos que
hacer una captura con retardo lo instalé con un *appimage*.

Shutter vuelve a estar disponible, al instalar desde este ppa ya queda
con las opciones de edición habilitadas:

``` {bash}
sudo add-apt-repository ppa:linuxuprising/shutter
sudo apt update
sudo apt install shutter
```

## Reoptimizar imágenes

### ImageMagick

Instalamos desde los repos, simplemente:

    sudo apt install imagemagick

### Imagine

Nos bajamos un *AppImage* desde el
[github](https://github.com/meowtec/Imagine/releases) de la aplicación

## dia

Un programa para crear diagramas

    sudo apt install dia dia-shapes gsfonts-x11

## Blender

Bajamos el Blender linkado estáticamente de [la página
web](https://www.blender.org) y lo descomprimimos en `~/apps/blender`.

## Structure Synth

Instalado desde repos, junto con sunflow para explorar un poco.

    sudo apt install structure-synth sunflow

## Heron animation

Descargamos el programa desde [su página
web](https://heronanimation.brunolefevre.net/) y como siempre
descomprimimos en `~/apps/heron`

## Stopmotion

Primero probamos el del repo: `sudo apt install stopmotion`

## Instalación del driver digiment para tabletas gráficas Huion

He intentado un par de veces instalar con el fichero `deb` pero parece
que no funciona.

Para hacer la instalación via DKMS el truco está en:

  - Dejar el código fuente en un directorio de la forma
    `/usr/src/<PROJECTNAME>-<VERSION>`
  - Lanzar el `build` pero usando esta vez `<PROJECTNAME>/<VERSION>`

Descargamos los últimos drivers desde [la página oficial de
releases](https://github.com/DIGImend/digimend-kernel-drivers/releases),
en el momento de escribir esto descargamos la versión V9.

Descomprimimos en `/usr/src/digimend-9`

``` {bash}
cd /usr/src
sudo xvzf <path-to-digimend-kernel-drivers-9> .
sudo dkms build digimend-kernel-drivers/9
sudo dkms install digimend/9
```

Para comprobar:

    xinput --list
    dkms status

Referencia:

  - [Aquí](https://davidrevoy.com/article331/setup-huion-giano-wh1409-tablet-on-linux-mint-18-1-ubuntu-16-04)

# Sonido

## Spotify

Spotify instalado desde las opciones de Linux Mint

## Audacity

Añadimos ppa:

    sudo add-apt-repository ppa:ubuntuhandbook1/audacity
    sudo apt-get update
    sudo apt install audacity

Instalamos también el plugin [Chris’s Dynamic Compressor
plugin](https://theaudacitytopodcast.com/chriss-dynamic-compressor-plugin-for-audacity/)

## Clementine

La version disponible en los orígenes de software parece al dia:

    sudo apt install clementine

# Video

## Shotcut

Nos bajamos la *AppImage* para Linux desde la [página
web](https://www.shotcut.org/).

La dejamos en `~/apps/shotcut` y:

    cd
    chmod 744 Shotcutxxxxxx.AppImage
    ./Shotcutxxxxxx.AppImage

## kdenlive

Está disponible como ppa o como *appimage*. Lo he bajado como *appimage*
para probarlo.

## Openshot

También descargado desde su web como *appimage*.

## Grabación de screencast

### Vokoscreen y Kazam

Instalados desde los repos oficiales:

    sudo apt update
    sudo apt install vokoscreen kazam

## Grabación de podcast

### Mumble

Instalamos desde PPA

    sudo add-apt-repository ppa:mumble/release
    sudo apt update
        sudo apt install mumble

# Fotografía

## Rawtherapee

Bajamos el AppImage desde la [página web](http://rawtherapee.com/) al
directorio `~/apps/rawtherapee`.

    cd
    chmod 744 RawTherapeexxxxxx.AppImage
    ./RawTherapeexxxxxx.AppImage

Al ejecutarla la primera vez ya se encarga la propia aplicación de
integrarse en nuestro sistema.

## Darktable

Instalamos ppa:

    sudo sh -c "echo 'deb http://download.opensuse.org/repositories/graphics:/darktable/xUbuntu_18.04/ /' > /etc/apt/sources.list.d/graphics:darktable.list"
    wget -nv https://download.opensuse.org/repositories/graphics:darktable/xUbuntu_18.04/Release.key -O Release.key
    sudo apt-key add - < Release.key
    sudo apt update
    sudo apt install darktable

Se instala la última versión de Darktable (3.0.0)

# Seguridad

## Autenticación en servidores por clave pública

Generar contraseñas para conexión servidores remotos:

    cd ~
    ssh-keygen -b 4096 [-t dsa | ecdsa | ed25519 | rsa | rsa1]
    cat .ssh/

Solo resta añadir nuestra clave pública en el fichero `authorized_keys`
del servidor remoto.

    cat ~/.ssh/id_xxx.pub | ssh user@hostname 'cat >> .ssh/authorized_keys'

[¿Cómo funciona
esto?](https://www.digitalocean.com/community/tutorials/understanding-the-ssh-encryption-and-connection-process)

## Claves gpg

`gpg --gen-key` Para generar nuestra clave.

  - **Siempre** hay que ponerle una fecha de expiración, la puedes
    cambiar más tarde.
  - **Siempre** hay que escoger la máxima longitud posible

## Seahorse

Para manejar todas nuestras claves con comodidad:

`sudo apt install seahorse`

## Conexión a github con claves ssh

Usando este método podemos conectarnos a github sin tener que teclear la
contraseña en cada conexión.

### Claves ssh

Podemos echar un ojo a nuestras claves desde `seahorse` la aplicación de
gestión de claves que hemos instalado. También podemos ver las claves
que tenemos generadas:

    ls -al ~/.ssh

En las claves listadas nuestras claves públicas aparecerán con extensión
`.pub`

También podemos comprobar que claves hemos añadido ya a nuestro agente
ssh con:

    ssh-add -l

Para generar una nueva pareja de claves ssh:

    ssh-keygen -t rsa -b 4096 -C "your_email@example.com"

Podremos dar un nombre distintivo a los ficheros de claves generados y
poner una contraseña adecuada a la clave. Si algún dia queremos cambiar
la contraseña:

    ssh-keygen -p

Ahora tenemos que añadir nuestra clave ssh en nuestra cuenta de github,
para ello editamos con nuestro editor de texto favorito el fichero
`~/.ssh/id_rsa.pub` y copiamos el contenido integro. Después pegamos ese
contenido en el cuadro de texto de la web de github.

Para comprobar que las claves instaladas en github funcionan
correctamente:

    ssh -T git@github.com
    Hi salvari! You've successfully authenticated, but GitHub does not provide shell access.

Este mensaje indica que todo ha ido bien.

Ahora en los repos donde queramos usar ssh debemos cambiar el remote:

    git remote set-url origin git@github.com:$USER/$REPONAME.git

## Signal

El procedimiento recomendado en la página oficial:

    curl -s https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add -
    echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list
    sudo apt update && sudo apt install signal-desktop

-----

**NOTA**: Parece que no funciona. Lo he instalado via *flatpack*

-----

## Lector DNI electrónico

Instalamos:

    sudo apt-get install pcscd pcsc-tools libccid

Como root ejecutamos pcsc\_scan:

    root@rasalhague:~# pcsc_scan 
    PC/SC device scanner
    V 1.4.23 (c) 2001-2011, Ludovic Rousseau <ludovic.rousseau@free.fr>
    Compiled with PC/SC lite version: 1.8.11
    Using reader plug'n play mechanism
    Scanning present readers...
    Waiting for the first reader...

Si insertamos el lector veremos algo como esto:

    root@rasalhague:~# pcsc_scan 
    PC/SC device scanner
    V 1.4.23 (c) 2001-2011, Ludovic Rousseau <ludovic.rousseau@free.fr>
    Compiled with PC/SC lite version: 1.8.11
    Using reader plug'n play mechanism
    Scanning present readers...
    Waiting for the first reader...found one
    Scanning present readers...
    0: C3PO LTC31 v2 (11061005) 00 00
    
    Wed Jan 25 01:17:20 2017
    Reader 0: C3PO LTC31 v2 (11061005) 00 00
      Card state: Card removed,

Si insertamos un DNI veremos que se lee la información de la tarjeta
insertada:

    Reader 0: C3PO LTC31 v2 (11061005) 00 00
      Card state: Card inserted, 

y mas rollo

Instalamos ahora el modulo criptográfico desde [este
enlace](https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1112)

Y además:

    aptitude install pinentry-gtk2 opensc

# Virtualizaciones y contenedores

## Instalación de *virtualBox*

Lo hacemos con los origenes de software oficiales (alternativamente,
podríamos hacerlo manualmente):

    # Importamos la clave gpg
    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    
    # Añadimos el nuevo origen de software
    sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(. /etc/os-release; echo "$UBUNTU_CODENAME") contrib"
    
    # Actualizamos la base de datos de paquetes
    sudo apt update

Ahora podemos comprobar que además del paquete *virtualbox* tenemos
varios paquetes con número de versión (p.ej. \_virtualbox.6.1), estos
últimos son los que hemos añadido (compruebalo con `apt-cache policy
[nombrepaquete]`)

Instalamos el que nos interesa:

``` {bash}
sudo apt install virtualbox-6.1
```

Descargamos también el [VirtualBox Extension
Pack](https://www.virtualbox.org/wiki/Downloads), este paquete lo
instalaremos desde el propio interfaz de usuario del *VirtualBox*. Y lo
instalamos con el comando:

    sudo VBoxManage extpack install ./Oracle_VM_VirtualBox_Extension_Pack-6.1.2.vbox-extpack 

Sólo nos queda añadir nuestro usuario al grupo `vboxusers`, con el
comando `sudo gpasswd -a username vboxusers`, y tendremos que cerrar la
sesión para refrescar nuestros grupos.

## qemu

Instalamos desde el repo oficial:

    sudo apt install qemu-kvm qemu virt-manager virt-viewer libvirt-bin

## Docker

Tenemos que añadir el repositorio correspondiente a nuestra
distribución:

    # Be safe
    sudo apt remove docker docker-engine docker.io
    sudo apt autoremove
    sudo apt update
    
    # Install pre-requisites
    sudo apt install ca-certificates curl
    
    # Import the GPG key
    
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    
    # Next, point the package manager to the official Docker repository
    
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
    
    # Update the package database
    
    sudo apt update
    #
    
    apt-cache policy docker-ce
    
    sudo apt install docker-ce
    
    sudo gpasswd -a salvari docker

Esto dejará el servicio *docker* funcionando y habilitado (arrancará en
cada reinicio del ordenador)

La forma de pararlo es:

``` {bash}
sudo systemctl stop docker
sudo systemctl disable docker
systemctl status docker
```

Añadimos el *bundle* **docker** en nuestro fichero `~/.zshrc` para tener
autocompletado en comandos de docker.

Para usar *docker* tendremos que arrancarlo, con los alias de nuestro
sistema para *systemd* ejecutamos:

``` {bash}
scst docker  # para arrancar el servicio

scsp docker  # para parar el servicio
```

### docker-compose

  - Nos bajamos la última versión disponible de [las releases de
    github](https://github.com/docker/compose/releases)
  - Movemos el fichero que hemos descargado a
    `/usr/local/bin/docker-compose`
  - Y le damos permisos de ejecución `sudo chmod +x
    /usr/local/bin/docker-compose`

# Utilidades para mapas y cartografía

## josm

Descargamos y añadimos la clave gpg:

    wget -q https://josm.openstreetmap.de/josm-apt.key -O- | sudo apt-key add -

Añadimos el origen de software:

    sudo add-apt-repository "deb [arch=amd64] https://josm.openstreetmap.de/apt $(. /etc/os-release; echo "$UBUNTU_CODENAME") universe"

Y ahora procedemos a la instalación:

    sudo apt update
    sudo apt install openjfx josm 

Alternativamente también podemos instalar la versión “nightly” con el
siguienete comando, pero tendréis actualizaciones diarias:

    sudo apt josm-latest

Ya estamos listos para editar Open Street Map offline.

## MOBAC

Bajamos el paquete desde [la página web](http://mobac.sourceforge.net/)
y descomprimimos en `~/apps/mobac` como de costumbre nos creamos una
entrada de menú con *MenuLibre*.

Conviene bajarse wms adicionales para MOBAC y leerse [la
wiki](http://mobac.sourceforge.net/wiki/index.php/Custom_XML_Map_Sources)

### Referencias

\*\[Cartografía digital\]
(https://digimapas.blogspot.com.es/2015/01/oruxmaps-vii-mapas-de-mobac.html)

## QGIS

Añadimos la clave gpg:

    wget -q  https://qgis.org/downloads/qgis-2017.gpg.key -O- | sudo apt-key add -

Ejecutamos:

    sudo add-apt-repository "deb [arch=amd64] https://qgis.org/debian $(. /etc/os-release; echo "$UBUNTU_CODENAME") main"

E instalamos como siempre

    sudo apt update
    sudo apt install qgis

### Referencias

  - [Conectar WMS con
    QGIS](https://mappinggis.com/2015/09/como-conectar-con-servicios-wms-y-wfs-con-arcgis-qgis-y-gvsig/)
  - [Importar OSM en
    QGIS](https://www.altergeosistemas.com/blog/2014/03/28/importando-datos-de-osm-en-qgis-2/)
  - [Learn OSM](http://learnosm.org/es/osm-data/osm-in-qgis/)
  - [QGIS
    Tutorials](http://www.qgistutorials.com/es/docs/downloading_osm_data.html)

# Recetas variadas

## Solucionar problemas de menús duplicados usando menulibre

|                                 |
| :------------------------------ |
| **Nota**: Ya no uso *MenuLibre* |

En el directorio `~/.config/menus/applications-merged` borramos todos
los ficheros que haya.

## Formatear memoria usb

“The driver descriptor says the physical block size is 2048 bytes, but
Linux says it is 512 bytes.”

Este comando borró todas las particiones de la memoria:

`sudo dd if=/dev/zero of=/dev/sdd bs=2048 count=32 && sync`

I’m assuming your using gparted.

First delete whatever partitions you can…just keep pressing ignore.

There will be one with a black outline…you will have to unmount it…just
right click on it and unmount.

Again you will have to click your way through ignore..if fix is an
option choose it also.

Once all this is done… you can select the device menu and choose new
partition table.

Select MSdos

Apply and choose ignore again.

Once it’s done it show it’s real size.

Next you can format the drive to whichever file system you like.

It’s a pain in the behind this way, but it’s the only way I get it
done..I put live iso’s on sticks all the time and have to remove them. I
get stuck going through this process every time.

## Copiar la clave pública ssh en un servidor remoto

`cat /home/tim/.ssh/id_rsa.pub | ssh tim@just.some.other.server 'cat >>
.ssh/authorized_keys'`

O también:

`ssh-copy-id -i ~/.ssh/id_rsa.pub username@remote.server`

## ssh access from termux

<https://linuxconfig.org/ssh-into-linux-your-computer-from-android-with-termux>

## SDR instalaciones varias

Vamos a trastear con un dispositivo
[RTL-SDR.com](https://www.rtl-sdr.com/).

Tenemos un montón de información en el blog de [SDR
Galicia](https://sdrgal.wordpress.com/) y tienen incluso una guia de
instalación muy completa, pero yo voy a seguir una guía un poco menos
ambiciosa, por lo menos hasta que pueda hacer el curso que imparten
ellos mismos (SDR Galicia)

La guía en cuestión la podemos encontrar
[aquí](https://ranous.wordpress.com/rtl-sdr4linux/)

Seguimos los pasos de instalación:

  - La instalación de `git`, `cmake` y `build-essential` ya la tengo
    hecha.

<!-- end list -->

    sudo apt-get install libusb-1.0-0-dev

## Posible problema con modemmanager y micros programables

Programando el *Circuit Playground Express* con el *Arduino IDE* tenía
problemas continuos para hacer los *uploads*, al parecer el servicio
*ModemManager* es el culpable, se pasa todo el tiempo capturando los
nuevos puertos serie por que considera que todo es un modem.

Una prueba rápida para comprobarlo: `sudo systemctl stop ModemManager`

Con esto funciona todo bien, pero en el siguiente arranque volvera a
cargarse.

Para dar una solución definitiva se puede programar una regla para
impedir que el *ModemManager* capture el puerto con un dispositivo

Creamos un fichero con permisos de `root` en el directorio
`/etc/udev/rules.d` que llamaremos: `99-arduino.rules`

Dentro de ese fichero especificamos los codigos VID/PID que se deben
ignorar:

    # for arduino brand, stop ModemManager grabbing port
    ATTRS{idVendor}=="2a03", ENV{ID_MM_DEVICE_IGNORE}="1"
    # for sparkfun brand, stop ModemManager grabbing port
    ATTRS{idVendor}=="1b4f", ENV{ID_MM_DEVICE_IGNORE}="1"

Ojo que si tienes SystemV no va a funcionar.

https://starter-kit.nettigo.eu/2015/serial-port-busy-for-avrdude-on-ubuntu-with-arduino-leonardo-eth/

https://www.codeproject.com/Tips/349002/Select-a-USB-Serial-Device-via-its-VID-PID

## Programar los nanos con chip ch340 o ch341

Linux mapea el chip correctamente en un puerto `/dev/ttyUSB0` y con eso
basta, que no te lien con el cuento de “drivers para linux”

Todo lo que hace falta es configurar correctamente el *Arduino IDE*, hay
que escoger:

    Board: "Arduino Nano"
    Processor: "ATmega168"
    Port: "/dev/ttyUSB0"

Y ya funciona todo.

1.  ya no incluye gksu pero tampoco es imprescindible

2.  Mantenemos aquí la lista de paquetes instalados en emacs aunque no
    todos son de desarrollo software
