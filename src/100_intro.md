---
title: Bitácora Linux Mint Tricia 19.3
author:
- Sergio Alvariño <salvari@gmail.com>
tags: [LinuxMint, Pandoc, Documentación, makefile, git]
date: enero-2020
lang: es-ES
abstract: |
  Bitácora de mi portatil
  
  Solo para referencia rápida y personal.
---

# Introducción

Mi portátil es un Lenovo Legion con las siguientes características:

* Core i7

* NVIDIA Geforce GTX

* 16Gb RAM

* 500Gb ssd


