# Desarrollo hardware

## Arduino IDE

Bajamos los paquetes de la página [web](https://www.arduino.cc),
descomprimimimos en _~/apps/arduino_.

La distribución del IDE incluye ahora un fichero `install.sh`que se encarga de
hacer la integración del IDE en los menús de Linux.

Además también incluye un script (`arduino-linux-setup.sh`) para crear
las _devrules_ y que además desinstala el driver _modemmanager_ y crea
grupos nuevos en el sistema si no existen.

No tengo claro lo de desinstalar el driver así que creamos las
_devrules_ a mano mirando por el fichero.



Hay que añadir nuestro usuario a los grupos _tty_, _dialout_, _uucp_ y
_plugdev_ (no hay que crear grupos nuevos, ya tenemos todos en el
sistema)

~~~~
sudo gpasswd --add <usrname> tty
sudo gpasswd --add <usrname> dialout
sudo gpasswd --add <usrname> uucp
sudo gpasswd --add <usrname> plugdev
~~~~

Creamos los siguientes ficheros en el directorio `/etc/udev/rules.d`

Fichero `90-extraacl.rules` mete mi usario en el fichero de reglas (¬_¬)

~~~~
# Setting serial port rules

KERNEL=="ttyUSB[0-9]*", TAG+="udev-acl", TAG+="uaccess", OWNER="salvari"
KERNEL=="ttyACM[0-9]*", TAG+="udev-acl", TAG+="uaccess", OWNER="salvari"
~~~~


Fichero `98-openocd.rules`

~~~~
# Adding Arduino M0/M0 Pro, Primo UDEV Rules for CMSIS-DAP port

ACTION!="add|change", GOTO="openocd_rules_end"
SUBSYSTEM!="usb|tty|hidraw", GOTO="openocd_rules_end"

#Please keep this list sorted by VID:PID

#CMSIS-DAP compatible adapters
ATTRS{product}=="*CMSIS-DAP*", MODE="664", GROUP="plugdev"

LABEL="openocd_rules_end"
~~~~

Fichero `avrisp.rules`

~~~~
# Adding AVRisp UDEV rules

SUBSYSTEM!="usb_device", ACTION!="add", GOTO="avrisp_end"
# Atmel Corp. JTAG ICE mkII
ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2103", MODE="660", GROUP="dialout"
# Atmel Corp. AVRISP mkII
ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2104", MODE="660", GROUP="dialout"
# Atmel Corp. Dragon
ATTR{idVendor}=="03eb", ATTRS{idProduct}=="2107", MODE="660", GROUP="dialout"

LABEL="avrisp_end"
~~~~

Fichero `40-defuse.rules`:

~~~~
# Adding STM32 bootloader mode UDEV rules

# Example udev rules (usually placed in /etc/udev/rules.d)
# Makes STM32 DfuSe device writeable for the "plugdev" group

ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE="664", GROUP="plugdev", TAG+="uaccess"
~~~~

Fichero `99-arduino-101.rules`:

~~~~
# Arduino 101 in DFU Mode

SUBSYSTEM=="tty", ENV{ID_REVISION}=="8087", ENV{ID_MODEL_ID}=="0ab6", MODE="0666", ENV{ID_MM_DEVICE_IGNORE}="1", ENV{ID_MM_CANDIDATE}="0"
SUBSYSTEM=="usb", ATTR{idVendor}=="8087", ATTR{idProduct}=="0aba", MODE="0666", ENV{ID_MM_DEVICE_IGNORE}="1"
~~~~

Yo añado el fichero `99-arduino.rules` que se encarga de inhibir el modemmanager para que no capture al _CircuitPlayground Express_:

~~~~
# for arduino brand, stop ModemManager grabbing port
ATTRS{idVendor}=="2a03", ENV{ID_MM_DEVICE_IGNORE}="1"
# for sparkfun brand, stop ModemManager grabbing port
ATTRS{idVendor}=="1b4f", ENV{ID_MM_DEVICE_IGNORE}="1"
~~~~

### Añadir soporte para _Feather M0_

Arrancamos el IDE Arduino y en la opción de _Preferences::Aditional
Boar Managers URLs_ añadimos la dirección
`https://adafruit.github.io/arduino-board-index/package_adafruit_index.json`,
si tenemos otras URL, simplemente añadimos esta separada por una coma.

Ahora desde el _Board Manager_ instalamos:

* Arduino SAMD Boards
* Adafruit SAMD Boards

### Añadir soporte para _Circuit Playground Express_

Bastaría con instalar _Arduino SAMD Boards_

### Añadir soporte para STM32

Tenemos varias URL posibles para configurar en las preferencias del IDE Arduino:

* <http://dan.drown.org/stm32duino/package_STM32duino_index.json> (recomendada por Tutoelectro)
* <https://github.com/stm32duino/BoardManagerFiles/raw/master/STM32/package_stm_index.json>
  (parece la oficial, y tiene mejor pinta)

### Añadir soporte para ESP32



### Añadir biblioteca de soporte para Makeblock

-------------------------------------------------------------------------------
__Nota__: Pendiente de instalar
-------------------------------------------------------------------------------


Clonamos el
[repo oficial en github](https://github.com/Makeblock-official/Makeblock-Libraries).

Una vez que descarguemos las librerias es necesario copiar el
directorio `Makeblock-Libraries/makeblock` en nuestro directorio de
bibliotecas de Arduino. En mi caso `~/Arduino/libraries/`.

Una vez instaladas las bibliotecas es necesario reiniciar el IDE
Arduino si estaba arrancado. Podemos ver si se ha instalado
correctamente simplemente echando un ojo al menú de ejemplos en el
IDE, tendríamos que ver los ejemplos de _Makeblock_.

Un detalle importante para programar el Auriga-Me es necesario
seleccionar el micro Arduino Mega 2560 en el IDE Arduino.


## Pinguino IDE

-------------------------------------------------------------------------------
__Nota__: Pendiente de instalar
-------------------------------------------------------------------------------

Tenemos el paquete de instalación disponible en su página
[web](http://pinguino.cc/download.php)

Ejecutamos el programa de instalación. El programa descargará los
paquetes Debian necesarios para dejar el IDE y los compiladores
instalados.

Al acabar la instalación he tenido que crear el directorio
_~/Pinguino/v11_, parece que hay algún problema con el programa de
instalación y no lo crea automáticamente.

El programa queda correctamente instalado en _/opt_ y arranca
correctamente, habrá que probarlo con los micros.

## esp-idf

Instalamos las dependencias (cmake ya lo tenemos instalado)

----------------------------------------------------------------------
__NOTA__: No es necesario instalar los paquetes de python que nos
especifican en las instrucciones de instalación del _esp-idf_, se
instalarán automáticamente en el siguiente paso.
----------------------------------------------------------------------

~~~~{bash}
sudo apt-get install gperf  cmake ninja-build ccache libffi-dev libssl-dev
~~~~

Ahora creamos un directorio para nuestro _tool-chain_:

~~~~{bash}
mkdir ~/esp
cd ~/esp
git clone --recursive https://github.com/espressif/esp-idf
~~~~

También es necesario que nuestro usuario pertenezca al grupo
`dialout`, pero eso ya deberíamos tenerlo hecho de antes.

Una vez clonado el repo ejecutamos el script de instalación

~~~~{bash}
cd ~/esp/esp-idf
./install.sh
~~~~

Este script nos va a dejar instaladas todas las herramientas
necesarias en el directorio `~/.expressif`

Para empezar a trabajar bastará con hacer un _source_ del fichero `~/esp/esp-idf/export.sh`:

~~~~{bash}
. ~/esp/esp-idf/export.sh
~~~~

## KiCAD

En la [página web del
proyecto](http://kicad-pcb.org/download/linux-mint/) nos recomiendan
el ppa a usar para instalar la última versión estable:

~~~~
sudo add-apt-repository --yes ppa:js-reynaud/kicad-5
sudo apt-get update
sudo apt-get install kicad
sudo apt install kicad-footprints kicad-libraries kicad-packages3d kicad-symbols kicad-templates
~~~~

Paciencia, el paquete `kicad-packages3d` tarda un buen rato en descargarse.

Algunas librerías alternativas:

* [Freetronics](https://github.com/freetronics/freetronics_kicad_library)
  una libreria que no solo incluye Shield para Arduino sino una
  completa colección de componentes que nos permitirá hacer proyectos
  completos. [Freetronics](http://www.freetronics.com) es una especie
  de BricoGeek australiano, publica tutoriales, vende componentes, y
  al parecer mantiene una biblioteca para KiCAD. La biblioteca de
  Freetronics se mantiene en un repo de github. Lo suyo es
  incorporarla a cada proyecto, por que si la actualizas se pueden
  romper los proyectos que estes haciendo.
* [eklablog](http://meta-blog.eklablog.com/kicad-librairie-arduino-pretty-p930786)
  Esta biblioteca de componentes está incluida en el github de KiCAD, así que
  teoricamente no habría que instalarla en nuestro disco duro.
  
## Analizador lógico

Mi analizador es un OpenBench de
Seedstudio,
[aquí hay mas info](http://dangerousprototypes.com/docs/Open_Bench_Logic_Sniffer)

### Sigrok

Instalamos __Sigrok__, simplemente desde los repos de Debian:

~~~~{bash}
sudo aptitude install sigrok
~~~~

Al instalar __Sigrok__ instalamos también __Pulseview__.

Si al conectar el analizador, echamos un ojo al
fichero _syslog_ vemos que al conectarlo se mapea en un puerto tty.

Si arrancamos __Pulseview__ (nuestro usuario tiene que estar incluido
en el grupo _dialout_), en la opción _File::Connect to device_,
escogemos la opción _Openbench_ y le pasamos el puerto.  Al pulsar la
opción _Scan for devices_ reconoce el analizador correctamente como un
_Sump Logic Analyzer_.

### Sump logic analyzer

Este es el software recomendado para usar con el analizador.

Descargamos el paquete de la [página del
proyecto](https://www.sump.org), o más concretamente de [esta
página](https://www.sump.org/projects/analyzer/) y descomprimimos en
_~/apps_.

Instalamos las dependencias:

~~~~{bash}
sudo apt install librxtx-java
~~~~

Editamos el fichero _~/apps/Logic Analyzer/client/run.sh_ y lo dejamos
así:

~~~~
#!/bin/bash

# java -jar analyzer.jar $*
java -cp /usr/share/java/RXTXcomm.jar:analyzer.jar org.sump.analyzer.Loader
~~~~

Y ya funciona.

### OLS

-------------------------------------------------------------------------------
__Nota__: Pendiente de instalar
-------------------------------------------------------------------------------

[Página oficial](https://www.lxtreme.nl/ols/)

## IceStudio

Instalamos dependencias con `sudo apt install xclip`

Bajamos el _AppImage_ desde el [github de
IceStudio](https://github.com/FPGAwars/icestudio) y lo dejamos en
`~/apps/icestudio`

## PlatformIO

### VS Code

Añadimos el origen de software:

~~~~
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
~~~~

E instalamos

~~~~
sudo apt update
sudo apt install code   # alternativamente code-insiders (es como la versión beta, se pueden instalar los dos)
~~~~

Ahora

1. lazamos el editor 
2. abrimos el gestor de extensiones
3. buscamos el platformio ide
4. instalamos

Seguimos las instrucciones de [aqui](https://docs.platformio.org/en/latest/ide/vscode.html#quick-start)

### Incluir platform.io CLI en el PATH

Modificamos el fichero `~/.profile` añadiendo las siguientes lineas:

~~~~
if [ -d "$HOME/.platformio/penv/bin"] ; then
    PATH="$PATH:$HOME/.platformio/penv/bin"
fi
~~~~

### Editor Atom


----------------------------------------------------------------------
_NOTA_: Parece que antes recomendaban instalar Atom para disponer del
Platformio CLI, ahora en cambio recomiendan VS Code.
----------------------------------------------------------------------

~~~~
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
sudo apt update
sudo apt install atom
~~~~

Completamos la instalación del paquete _Platformio IDE_ desde el Atom siguiendo las
instrucciones
[aquí](https://platformio.org/get-started/ide?install=atom).

Para poder usar _platformio_ sin usar _Atom_ añadimos la siguiente
linea al fichero `.profile`:

~~~~
export PATH=$PATH:~/.platformio/penv/bin
~~~~

* [Referencia](https://docs.platformio.org/en/latest/installation.html#piocore-install-shell-commands)



## RepRap

### OpenScad

El OpenSCAD _no está_ disponible en los orígenes de software, es necesario añadir un ppa:

~~~~
sudo add-apt-repository ppa:openscad/releases
sudo apt update
sudo apt install openscad
~~~~

### Slic3r

Descargamos la estable desde la [página web](https://dl.slic3r.org) y
como de costumbre descomprimimos en `~/apps` y creamos un lanzador con
_MenuLibre_

### Slic3r Prusa Edition

Una nueva versión del clásico _Slic3r_ con muchas mejoras. Descargamos
la _appimage_ desde la [página
web](https://www.prusa3d.com/slic3r-prusa-edition/) y ya sabeis,
descomprimir en `~/apps` y dar permisos de ejecución.

### ideaMaker

Una aplicación más para generar gcode con muy buena pinta, tenemos el
paquete _deb_ disponible en su [página
web](https://www.raise3d.com/pages/ideamaker). Instalamos con el
gestor de software.

### Ultimaker Cura

Descargamos el _AppImage_ desde la [página
web](https://github.com/Ultimaker/Cura/releases)

### Pronterface

Seguimos las instrucciones para Ubuntu Bionic:

Instalamos las dependencias:

Clonamos el repo:

~~~~
cd ~/apps
git clone https://github.com/kliment/Printrun.git
cd Printrun
mkvirtualenv -p /usr/bin/python3 printrun
python -m pip install https://extras.wxpython.org/wxPython4/extras/linux/gtk2/ubuntu-18.04/wxPython-4.0.7-cp36-cp36m-linux_x86_64.whl

sudo apt-get install libdbus-glib-1-dev libdbus-1-dev

~~~~

Y ya lo tenemos todo listo para ejecutar.

## Cortadora de vinilos

### Inkcut

Instalado en un entorno virtual:

~~~~{bash}
mkvirtualenv -p `which python3` inkcut

sudo apt install libxml2-dev libxslt-dev libcups2-dev

pip install PyQt5

pip install inkcut
~~~~

### Plugin para inkscape

Instalamos dependencias:

~~~~{bash}
pip install python-usb
~~~~

Instalamos el fichero `.deb` desde la web
<https://github.com/fablabnbg/inkscape-silhouette/releases>
