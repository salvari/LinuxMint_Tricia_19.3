# Sonido

## Spotify

Spotify instalado desde las opciones de Linux Mint

## Audacity

Añadimos ppa:

~~~~
sudo add-apt-repository ppa:ubuntuhandbook1/audacity
sudo apt-get update
sudo apt install audacity
~~~~

Instalamos también el plugin [Chris’s Dynamic Compressor
plugin](https://theaudacitytopodcast.com/chriss-dynamic-compressor-plugin-for-audacity/)

## Clementine

La version disponible en los orígenes de software parece al dia:

~~~~
sudo apt install clementine
~~~~

# Video

## Shotcut

Nos bajamos la _AppImage_ para Linux desde la [página
web](https://www.shotcut.org/).

La dejamos en `~/apps/shotcut` y:

~~~~
cd
chmod 744 Shotcutxxxxxx.AppImage
./Shotcutxxxxxx.AppImage
~~~~

## kdenlive

Está disponible como ppa o como _appimage_. Lo he bajado como
_appimage_ para probarlo.

## Openshot

También descargado desde su web como _appimage_.



## Grabación de screencast

### Vokoscreen y Kazam

Instalados desde los repos oficiales:

~~~~
sudo apt update
sudo apt install vokoscreen kazam
~~~~

## Grabación de podcast

### Mumble

Instalamos desde PPA

~~~~
sudo add-apt-repository ppa:mumble/release
sudo apt update
    sudo apt install mumble
~~~~


# Fotografía

## Rawtherapee

Bajamos el AppImage desde la [página web](http://rawtherapee.com/) al
directorio `~/apps/rawtherapee`.

~~~~
cd
chmod 744 RawTherapeexxxxxx.AppImage
./RawTherapeexxxxxx.AppImage
~~~~

Al ejecutarla la primera vez ya se encarga la propia aplicación de
integrarse en nuestro sistema.


## Darktable

Instalamos ppa:

~~~~
sudo sh -c "echo 'deb http://download.opensuse.org/repositories/graphics:/darktable/xUbuntu_18.04/ /' > /etc/apt/sources.list.d/graphics:darktable.list"
wget -nv https://download.opensuse.org/repositories/graphics:darktable/xUbuntu_18.04/Release.key -O Release.key
sudo apt-key add - < Release.key
sudo apt update
sudo apt install darktable
~~~~


Se instala la última versión de Darktable (3.0.0)


