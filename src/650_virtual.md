# Virtualizaciones y contenedores

## Instalación de _virtualBox_

Lo hacemos con los origenes de software oficiales (alternativamente, podríamos hacerlo manualmente):

~~~~
# Importamos la clave gpg
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -

# Añadimos el nuevo origen de software
sudo add-apt-repository "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(. /etc/os-release; echo "$UBUNTU_CODENAME") contrib"

# Actualizamos la base de datos de paquetes
sudo apt update
~~~~

Ahora podemos comprobar que además del paquete _virtualbox_ tenemos
varios paquetes con número de versión (p.ej. _virtualbox.6.1), estos
últimos son los que hemos añadido (compruebalo con `apt-cache policy
[nombrepaquete]`)

Instalamos el que nos interesa:

~~~~{bash}
sudo apt install virtualbox-6.1
~~~~

Descargamos también el [VirtualBox Extension
Pack](https://www.virtualbox.org/wiki/Downloads), este paquete lo
instalaremos desde el propio interfaz de usuario del _VirtualBox_. Y
lo instalamos con el comando:

~~~~
sudo VBoxManage extpack install ./Oracle_VM_VirtualBox_Extension_Pack-6.1.2.vbox-extpack 
~~~~


Sólo nos queda añadir nuestro usuario al grupo `vboxusers`, con el
comando `sudo gpasswd -a username vboxusers`, y tendremos que cerrar
la sesión para refrescar nuestros grupos.

  
## qemu

Instalamos desde el repo oficial:

~~~~
sudo apt install qemu-kvm qemu virt-manager virt-viewer libvirt-bin
~~~~


## Docker

Tenemos que añadir el repositorio correspondiente a nuestra
distribución:

~~~~
# Be safe
sudo apt remove docker docker-engine docker.io
sudo apt autoremove
sudo apt update

# Install pre-requisites
sudo apt install ca-certificates curl

# Import the GPG key

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Next, point the package manager to the official Docker repository

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"

# Update the package database

sudo apt update
#

apt-cache policy docker-ce

sudo apt install docker-ce

sudo gpasswd -a salvari docker
~~~~

Esto dejará el servicio _docker_ funcionando y habilitado (arrancará
en cada reinicio del ordenador)

La forma de pararlo es:

~~~~{bash}
sudo systemctl stop docker
sudo systemctl disable docker
systemctl status docker
~~~~

Añadimos el _bundle_ __docker__ en nuestro fichero `~/.zshrc` para
tener autocompletado en comandos de docker.

Para usar _docker_ tendremos que arrancarlo, con los alias de nuestro sistema para _systemd_ ejecutamos:

~~~~{bash}
scst docker  # para arrancar el servicio

scsp docker  # para parar el servicio
~~~~

### docker-compose

* Nos bajamos la última versión disponible de [las releases de
  github](https://github.com/docker/compose/releases)
* Movemos el fichero que hemos descargado a `/usr/local/bin/docker-compose`
* Y le damos permisos de ejecución `sudo chmod +x /usr/local/bin/docker-compose`


